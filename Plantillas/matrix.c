#include "matrix.h"

void matbuild(int *ptr_fil, int *ptr_col, double ***ptr_matrix)
{

    double **matrix;
    int i, j, k;

    printf(" ║ Ingresa el número de filas de la matriz: ");
    scanf("%d", ptr_fil);
    printf(" ║ \n");
    printf(" ║ Ingresa el número de columnas de matriz: ");
    scanf("%d", ptr_col);
    printf(" ║ \n");

    matrix = (double **)calloc(*ptr_fil, sizeof(double));
    if (!matrix)
        return NULL;
    for (i = 0; i < *ptr_fil; i++)
    {
        matrix[i] = (double *)calloc(*ptr_col, sizeof(double));
        if (!matrix[i])
            return NULL;
    }

    for (i = 0; i < *ptr_fil; i++)
    {
        for (j = 0; j < *ptr_col; j++)
        {
            printf(" ║ Ingresa el valor de la posición (%d,%d): ", i + 1, j + 1);
            scanf("%d", &matrix[i][j]);
        }
        printf(" ║ \n");
    }
    *ptr_matrix = matrix;
}

void matprint(int *ptr_fil, int *ptr_col, double **ptr_matrix)
{

    for (int i = 0; i < *ptr_fil; i++)
    {

        printf(" ║ |");
        for (int j = 0; j < *ptr_col; j++)
        {
            printf(" %02d", ptr_matrix[i][j]);
        }
        printf(" |\n");

    }

    printf(" ║ \n");

}