/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 03-02-2022
 * Última modificación: 16-02-2022
 *
 * Descripción: Biblioteca practica0.h para la declaración
 * de funciones relativas a los ejercicios de las prácti-
 * cas.
 *
 * Últimos cambios: Nuevos menús de navegación para ejer-
 * cicios complejos (menupi y menups) y delimitadores para
 * la entrada y salida de datos, además de la finalización
 * de ciertos procesos.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "nav.h"

#ifndef _PRACTICA7_H_
#define _PRACTICA7_H_

struct node{

    char info[40];
    struct node *next_ptr;

};

struct course_node{

    char course[40];
    char group;
    char classroom[5];
    char schedule[15];
    struct course_node *nextcourse_ptr;

};



void ejercicio1();
void ejercicio2();

#endif