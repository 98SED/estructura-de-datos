/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 00-00-2022
 *
 * Descripción: 
 * 
 * 
 */

/** Borrar hasta marcar todos los pasos:
 * TODO: Ajustar nombres de archivos. [x]
 * TODO: Ajustar el número de practica en #include "practica0.h" dentro de los archivos: practicaN.c, ejercicioNN_NN.c [x]
 * TODO: Ajustar las indicaciones de prepocesador en el archivo practicaN.h [x]
 * TODO: Añadir descripciones a los comentarios iniciales y ajustar las fechas [?]
 * TODO: Ajustar la fecha y número de practica/ejercicios de las funciones bienv() y bienvprog() [?]
 * TODO: Declarar las funciones de cada uno de los ejercicios en practicaN.h [?]
 * TODO: Crear los archivos necesarios para cada uno de los ejercicios. [x]
 */

#include "practica7.h"

int main()
{

    int opt;

    bienv("15-03-2022", "Practica 7 "); /* 10 , 11 */

    do
    {
        menpri(" 1 - Ejercicio 1 ", " 2 - Ejercicio 2 ", "                 ", "                 ", "                 ", &opt); /* 17 */

        switch (opt)
        {
        case 1:
            ejercicio1();
            break;
        case 2:
            ejercicio2();
            break;
        case 3:
            //ejercicio3();
            break;
        case 4:
            // ejercicio4();
            break;
        case 5:
            // ejercicio5();
            break;
        case 6:
            break;
        default:
            msjerr("             La opción ingresada no es valida            ", "               Ingresa una opción valida...              "); /* 57 */
            break;
        }

    } while (opt != 6);

msjfin();

return 0;

}