/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 00-00-2022
 *
 * Descripción:
 *
 *
 */

#include "practica7.h"

void insert_course_node();
void print_course_nodes();

struct course_node *origin_course_ptr;

void ejercicio2()
{

    // Declaraciones
    int elements, opt;

    // Mensaje inicial al usuario
    bienvprog3("  Ejercicio 2  ", "En el presente ejercicio se pone en practica",
                                  "el concepto de listas de tipo pila, donde el primer      ",
                                  "elemento que se ingresa es el último de la lista         "); /* 15, 44, 57 */


    do
    {
        menprog("1 - Añadir elem. ", "2 - Imp elementos", "   3 - Regresar  ", &opt);

        switch (opt)
        {
        case 1:
            // Entrada
            msjent();
            insert_course_node();
            break;
        case 2:
            // Salida
            print_course_nodes();
            break;
        case 3:
            break;
        default:
            msjerr("            La opción ingresada no es valida            ", "                Ingrese una opción valida               ");
            break;
        }

    } while (opt != 3);

    msjfinprog();
}

void insert_course_node()
{

    struct course_node *new_node_ptr;

    new_node_ptr = malloc(sizeof(struct course_node));

    msjpros1("Ingresa los datos solicitados para guardar la asignatura ");
    printf(" ║ Nombre de la Asignatura (40 car. max.):");
    printf("\n ║ ");
    getc(stdin);
    fgets(new_node_ptr->course, 40, stdin);
    fflush(stdin);

    printf(" ║ Grupo (A-Z): ");
    scanf(" %c", &new_node_ptr->group);
    getchar();
    fflush(stdin);

    printf(" ║ Salón (X000):");
    printf("\n ║ ");
    fgets(new_node_ptr->classroom, 5, stdin);
    fflush(stdin);

    printf(" ║ Horario (00:00-00:00): ");
    printf("\n ║ ");
    fgets(new_node_ptr->schedule, 15, stdin);
    fflush(stdin);
    
    if (origin_course_ptr == NULL)
    {
        origin_course_ptr = new_node_ptr;
        new_node_ptr->nextcourse_ptr = NULL;
    }
    else
    {
        new_node_ptr->nextcourse_ptr = origin_course_ptr;
        origin_course_ptr = new_node_ptr;
    }
}

void print_course_nodes()
{

    struct course_node *node_ptr = origin_course_ptr;
    int i = 1;

    msjsal();
    msjpros1("        La información ingresada es la siguiente:        ");

    while (node_ptr != NULL)
    {

        printf(" ║ ========================================================= ║\n");
        printf(" ║                                                           ║\n");
        printf(" ║ Asignatura: %s", node_ptr->course);
        printf(" ║ Grupo: %c\n", node_ptr->group);
        printf(" ║ Salón: %s", node_ptr->classroom);
        printf(" ║ Horario: %s", node_ptr->schedule);
        printf(" ║                                                           ║\n");
        printf(" ║ ========================================================= ║\n");

        node_ptr = node_ptr->nextcourse_ptr;
    }
}

/**
 * printf(" ║                                                           ║\n");
 *
 * printf(" ╠═══════════════════════════════════════════════════════════╣\n");
 *
 * printf(" ║ ========================================================= ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║ ========================================================= ║\n");
 *
 * printf(" ║ % ║ \n", var);
 *
 * printf(" ║ % \n", var);
 *
 * printf(" ║ \n");
 *
 * printf(" ║ Ingresa una opción: ");
 * scanf("%", var);
 *
 * printf(" ║ Ingresa una opción: ");
 * fgets(var, 0, stdin);
 */