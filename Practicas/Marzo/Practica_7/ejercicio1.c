/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 00-00-2022
 *
 * Descripción:
 *
 *
 */

#include "practica7.h"

void insert_node();
void print_nodes();

struct node *origin_ptr;

void ejercicio1()
{

    // Declaraciones
    int elements, opt;

    // Mensaje inicial al usuario
    bienvprog3("  Ejercicio 1  ", "En el presente ejercicio se pone en practica",
                                  "el concepto de listas de tipo pila, donde el primer      ",
                                  "elemento que se ingresa es el último de la lista         "); /* 15, 44, 57 */


    do
    {
        menprog("1 - Añadir elem. ", "2 - Imp elementos", "   3 - Regresar  ", &opt);

        switch (opt)
        {
        case 1:
            // Entrada
            msjent();
            printf("Ingresa número de nodos: ");
            scanf("%d", &elements);
            getc(stdin);
            for (size_t i = 0; i < elements; i++)
            {
                insert_node();
            }
            break;
        case 2:
            // Salida
            print_nodes();
            break;
        case 3:
            break;
        default:
            msjerr("            La opción ingresada no es valida            ", "                Ingrese una opción valida               ");
            break;
        }

    } while (opt != 3);

    msjfinprog();
}

void insert_node()
{

    struct node *new_node_ptr;

    new_node_ptr = malloc(sizeof(struct node));

    printf(" ║ Ingresa la información que se desea guardar: ");
    printf("\n ║ ");
 //   getc(stdin);
    fgets(new_node_ptr->info, 41, stdin);
    fflush(stdin);

    if (origin_ptr == NULL)
    {
        origin_ptr = new_node_ptr;
        new_node_ptr->next_ptr = NULL;
    }
    else
    {
        new_node_ptr->next_ptr = origin_ptr;
        origin_ptr = new_node_ptr;
    }
}

void print_nodes()
{

    struct node *node_ptr = origin_ptr;
    int i = 1;

    msjsal();
    msjpros1("        La información ingresada es la siguiente:        ");

    while (node_ptr != NULL)
    {

        printf(" ║ ========================================================= ║\n");
        printf(" ║                                                           ╝\n");
        printf(" ║                        Elemento %d                        \n", i);
        printf(" ║ %s", node_ptr->info);
        printf(" ║                                                           ╗\n");
        printf(" ║ ========================================================= ║\n");
        i++;

        node_ptr = node_ptr->next_ptr;
    }
}

/**
 * printf(" ║                                                           ║\n");
 *
 * printf(" ╠═══════════════════════════════════════════════════════════╣\n");
 *
 * printf(" ║ ========================================================= ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║ ========================================================= ║\n");
 *
 * printf(" ║ % ║ \n", var);
 *
 * printf(" ║ % \n", var);
 *
 * printf(" ║ \n");
 *
 * printf(" ║ Ingresa una opción: ");
 * scanf("%", var);
 *
 * printf(" ║ Ingresa una opción: ");
 * fgets(var, 0, stdin);
 */
