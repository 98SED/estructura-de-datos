/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 22-03-2022
 *
 * Descripción: El presente programa pretende poner en
 * practica el uso de listas de tipo pila y cola, jun-
 * to a otras funciones auxiliares que periten visua-
 * lizar el contenido de las listas o eliminar cier-
 * tos elementos de estas.
 */

#include "practica8.h"

void insert_course_node_stack();
void insert_course_node_tail();
void remove_course_node();
void print_course_nodes();

struct course_node *origin_course_ptr;
struct course_node *last_course_ptr;

void ejercicio1()
{

    // Declaraciones
    
    int elements, opt, opt2 = 0;

    // Mensaje inicial al usuario
    bienvprog4("  Ejercicio 2  ", "En el presente ejercicio se pone en practica",
                                  "el concepto de listas de tipo pila y cola.               ",
                                  "A continuación debera ingresar información               ",
                                  "correspondiente a un horario académico.                  "); /* 15, 44, 57 */


    do
    {
        printf(" ╠═══════════════════╦═══════════════════╦═══════════════════╣\n");
        menpri("1 - Añadir elem. ", "2 - Imp elementos", " 3 - Elim. Elem. ", "   4 - Regresar  ", "                 ", &opt);

        switch (opt)
        {
        case 1:
            // Entrada
            msjent();
            insert_course_node(&opt2);
            break;
        case 2:
            // Salida
            print_course_nodes();
            break;
        case 3:
            remove_course_node();
            break;
        case 4:
            break;
        case 6:
            return 0;
            break;
        default:
            msjerr("            La opción ingresada no es valida            ", "                Ingrese una opción valida               ");
            break;
        }

    } while (opt != 4);

    msjfinprog();
}

void insert_course_node(int *opt_ptr)
{

    int elements;

    if (*opt_ptr == 0)
    {

        do
        {
            msjpros2("Seleccione el método de entrada para el ingreso de datos:", "      NOTA: UNA VEZ SELECCIONADO NO SE PUEDE CAMBIAR     ");
            menprog("     1 - Pila    ", "     2 - Cola    ", "                 ", opt_ptr);

            switch (*opt_ptr)
            {
                case 1:
                    msjpros1("              El método seleccionado es Pila             ");
                    break;
                case 2:
                    msjpros1("              El método seleccionado es Cola             ");
                    break;
                default:
                    msjerr("            La opción ingresada no es valida            ", "                Ingrese una opción valida               ");
                    break;
            }

        } while (*opt_ptr != 1 && *opt_ptr != 2);

    }

    switch (*opt_ptr)
    {
    case 1:
        entint("Ingresa la cantidad de elementos a guardar", &elements);
        getc(stdin);
        for (size_t i = 0; i < elements; i++)
        {
            insert_course_node_stack();
        }
        break;
    case 2:
        entint("Ingresa la cantidad de elementos a guardar", &elements);
        getc(stdin);
        for (size_t i = 0; i < elements; i++)
        {
            insert_course_node_tail();
        }
        break;
    default:
        msjerr("            La opción ingresada no es valida            ", "                Ingrese una opción valida               ");
        break;
    }


}

void insert_course_node_stack()
{

    struct course_node *new_node_ptr;

    new_node_ptr = malloc(sizeof(struct course_node));

    msjpros1("Ingresa los datos solicitados para guardar la asignatura ");
    printf(" ║ Nombre de la Asignatura (40 car. max.):");
    printf("\n ║ ");
//    getc(stdin);
    fgets(new_node_ptr->course, 40, stdin);
    fflush(stdin);

    printf(" ║ Grupo (A-Z): ");
    scanf(" %c", &new_node_ptr->group);
    getchar();
//    fflush(stdin);

    printf(" ║ Salón (X000):");
    printf("\n ║ ");
    fgets(new_node_ptr->classroom, 5, stdin);
    fflush(stdin);

    printf(" ║ Horario (00:00-00:00): ");
    printf("\n ║ ");
    fgets(new_node_ptr->schedule, 15, stdin);
    fflush(stdin);

    if (origin_course_ptr == NULL)
    {
        origin_course_ptr = new_node_ptr;
        new_node_ptr->nextcourse_ptr = NULL;
    }
    else
    {
        new_node_ptr->nextcourse_ptr = origin_course_ptr;
        origin_course_ptr = new_node_ptr;
    }
}

void insert_course_node_tail()
{

    struct course_node *new_node_ptr;

    new_node_ptr = malloc(sizeof(struct course_node));

    msjpros1("Ingresa los datos solicitados para guardar la asignatura ");
    printf(" ║ Nombre de la Asignatura (40 car. max.):");
    printf("\n ║ ");
//    getc(stdin);
    fgets(new_node_ptr->course, 40, stdin);
    fflush(stdin);

    printf(" ║ Grupo (A-Z): ");
    scanf(" %c", &new_node_ptr->group);
    getchar();
//    fflush(stdin);

    printf(" ║ Salón (X000):");
    printf("\n ║ ");
    fgets(new_node_ptr->classroom, 5, stdin);
    fflush(stdin);

    printf(" ║ Horario (00:00-00:00): ");
    printf("\n ║ ");
    fgets(new_node_ptr->schedule, 15, stdin);
    fflush(stdin);

    new_node_ptr->nextcourse_ptr = NULL;

    if (origin_course_ptr == NULL)
    {
        origin_course_ptr = new_node_ptr;
        last_course_ptr = new_node_ptr;
    }
    else
    {
        last_course_ptr->nextcourse_ptr = new_node_ptr;
        last_course_ptr = new_node_ptr;
    }
}

void remove_course_node()
{

    struct course_node *node_ptr = origin_course_ptr;

    struct course_node *aux = origin_course_ptr;

    struct course_node *previous_node = NULL;

    char value[40];

    msjadv(" La función se encuentra en mantenimiento y su funciona- ", "  miento puede ser incorrecto bajo ciertas condiciones.  ");
    printf(" ║ Ingrese la asignatura que se desea borrar:                ║");
    printf("\n ║ ");
    getchar();
    fgets(value, 40, stdin);
    fflush(stdin);

    while( (aux != NULL) && (aux->course == value) )
    {
        previous_node = aux;
        aux = aux->nextcourse_ptr;
    }
    if( aux != NULL )
    {
        if( previous_node!=NULL )
        {
            previous_node->nextcourse_ptr = aux->nextcourse_ptr;
        }
        else
        {
            origin_course_ptr = aux->nextcourse_ptr;
        }

        free(aux);

    }

}

void print_course_nodes()
{

    struct course_node *node_ptr = origin_course_ptr;

    msjsal();
    msjpros1("        La información ingresada es la siguiente:        ");

    while (node_ptr != NULL)
    {

        printf(" ║ ========================================================= ║\n");
        printf(" ║                                                           ║\n");
        printf(" ║ Asignatura: %s", node_ptr->course);
        printf(" ║ Grupo: %c\n", node_ptr->group);
        printf(" ║ Salón: %s", node_ptr->classroom);
        printf(" ║ Horario: %s", node_ptr->schedule);
        printf(" ║                                                           ║\n");
        printf(" ║ ========================================================= ║\n");

        node_ptr = node_ptr->nextcourse_ptr;
    }
}

/**
 * printf(" ║                                                           ║\n");
 *
 * printf(" ╠═══════════════════════════════════════════════════════════╣\n");
 *
 * printf(" ║ ========================================================= ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║ ========================================================= ║\n");
 *
 * printf(" ║ % ║ \n", var);
 *
 * printf(" ║ % \n", var);
 *
 * printf(" ║ \n");
 *
 * printf(" ║ Ingresa una opción: ");
 * scanf("%", var);
 *
 * printf(" ║ Ingresa una opción: ");
 * fgets(var, 0, stdin);
 */