/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 22-03-2022
 *
 * Descripción: El presente programa pretende poner en
 * practica el uso de listas de tipo pila y cola, jun-
 * to a otras funciones auxiliares que periten visua-
 * lizar el contenido de las listas o eliminar cier-
 * tos elementos de estas.
 */

#include "practica8.h"

int main()
{

    int opt;

    bienv("22-03-2022", "Practica 8 "); /* 10 , 11 */

    do
    {
        menpri(" 1 - Ejercicio 1 ", "                 ", "                 ", "                 ", "                 ", &opt); /* 17 */

        switch (opt)
        {
        case 1:
            ejercicio1();
            break;
        case 6:
            break;
        default:
            msjerr("             La opción ingresada no es valida            ", "               Ingresa una opción valida...              "); /* 57 */
            break;
        }

    } while (opt != 6);

msjfin();

return 0;

}