/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 03-02-2022
 * Última modificación: 16-02-2022
 *
 * Descripción: Biblioteca general.h para menús de navega-
 * ción CUI para emuladores de terminal.
 *
 * Últimos cambios: Nuevos menús de navegación para ejer-
 * cicios complejos (menupi y menups) y delimitadores para
 * la entrada y salida de datos, además de la finalización
 * de ciertos procesos.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef _NAV_H_
#define _NAV_H_

// Mensajes de bienvenida
void bienv(char str_fecha[11], char str_actividad[12]);
void bienvprog1(char str_programa[16], char str_descln1[45]);
void bienvprog2(char str_programa[16], char str_descln1[45], char str_descln2[58]);
void bienvprog3(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58]);
void bienvprog4(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58], char str_descln4[58]);
void bienvprog5(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58], char str_descln4[58], char str_descln5[58]);
void bienvprog6(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58], char str_descln4[58], char str_descln5[58], char str_descln6[58]);


void msjent();
void msjsal();
void msjfin();
void msjfinprog();
void msjerr(char causa[58], char solucion[58]);
void msjadv(char causa[58], char solucion[58]);
void msjpros1(char msj[58]);
void msjpros2(char msj1[58], char msj2[58]);


void menpri(char str_cas1[18], char str_cas2[18], char str_cas3[18], char str_cas4[18], char str_cas5[18], int *ptr_opt);
void menprog(char str_cas1[18], char str_cas2[18], char str_cas3[18], int *ptr_opt);


void lnb();
void lns();
void lnstr(char str[57]);
void lnstra(char str[57]);
void slt();
void entint(char str[57], int *ptr_int);
void entfloat(char str[57], float *ptr_float);
void entdouble(char str[57], double *ptr_double);
// void entstring(char str[57], char *entstr, int longitud);

#endif