/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 29-03-2022
 *
 * Descripción: En el presente programa se ponen en practica
 * las listas doblemente enlazadas y circulares con el ejer-
 * cicio que se ha ido desarrollando desde la practica 7
 * en el cual el usuario debe capturar datos correspondien-
 * tes a las asignaturas de un horario académico, con la
 * opción de seleccionar el método a través del cual se
 * almacena la información (tipo de lista).
 */

#include "practica9.h"
#include "lists.h"

void ejercicio1()
{

    // Declaraciones
    int opt, list_opt, control;
    list_t list = NULL;

    // Mensaje inicial al usuario
    bienvprog4("  Ejercicio 1  ", "En el programa se solicita información      ",
                                "correspondiente a las asignaturas de un horario          ",
                                "académico, para ser almacenada de una lista a través del ",
                                "método seleccionado por el usuario.                      "); /* 15, 44, 57 */

    if (control != 1)
    {
        menprogext("1 - Pila", "2 - Cola", "  3 - Circular  ", " 4 - Doble enlace", "5 Dbl. enlc. cir.", "                 ", &opt);
        do
        {
            switch (opt)
            {
            case 1:
                /* code */
                break;

            case 2:
                /* code */
                break;

            case 3:
                list_opt = 3;
                control = 1;
                break;

            case 4:
                /* code */
                break;

            case 5:
                /* code */
                break;

            case 6:
                /* code */
                break;

            default:
                break;
            }
        } while (opt != 6);

    }

    do
    {
        menprogext("  1 - Nuevo nodo ", "  2 - Mod. nodo  ", "3 - Eliminar nodo", "  4 - Imp. lista ")

        switch (opt)
        {
        case 1:
            newnode(&list);
            break;
        
        default:
            break;
        }
    } while (opt =! 4);
    



    // Entrada
    msjent();


    // Proceso y salida
    msjsal();


    msjfinprog();


}

/**
 * printf(" ║                                                           ║\n");
 *
 * printf(" ╠═══════════════════════════════════════════════════════════╣\n");
 *
 * printf(" ║ ========================================================= ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║ ========================================================= ║\n");
 *
 * printf(" ║ % ║ \n", var);
 *
 * printf(" ║ % \n", var);
 *
 * printf(" ║ \n");
 *
 * printf(" ║ Ingresa una opción: ");
 * scanf("%", var);
 *
 * printf(" ║ Ingresa una opción: ");
 * fgets(var, 0, stdin);
 */