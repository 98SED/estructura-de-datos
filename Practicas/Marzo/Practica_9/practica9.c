/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 29-03-2022
 *
 * Descripción: En el presente programa se ponen en practica
 * las listas doblemente enlazadas y circulares con el ejer-
 * cicio que se ha ido desarrollando desde la practica 7
 * en el cual el usuario debe capturar datos correspondien-
 * tes a las asignaturas de un horario académico, con la
 * opción de seleccionar el método a través del cual se
 * almacena la información (tipo de lista).
 */

#include "practica9.h"

int main()
{

    int opt;

    bienv("28-03-2022", "Practica 9 "); /* 10 , 11 */

    do
    {
        menpri(" 1 - Ejercicio 1 ", "                 ", "                 ", "                 ", "                 ", &opt); /* 17 */

        switch (opt)
        {
        case 1:
            ejercicio1();
            break;
        case 6:
            break;
        default:
            msjerr("             La opción ingresada no es valida            ", "               Ingresa una opción valida...              "); /* 57 */
            break;
        }

    } while (opt != 6);

msjfin();

return 0;

}