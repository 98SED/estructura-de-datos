#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "nav.h"

#ifndef _LISTS_H_
#define _LISTS_H_

typedef struct course_node_str{

    char course[40];
    char group;
    char classroom[5];
    char schedule[15];

    /*course_node_t *prevnode_ptr;*/

    struct course_node_str *nextnode_ptr;

}course_node_t;

typedef course_node_t *first_node_t;
typedef course_node_t *list_t;

void newnodestack();
void newnodetail();
void newnodec(list_t *list);
void newnodedll();
void newnodedcll();
void newnode(int *list_opt);

void printls();

#endif