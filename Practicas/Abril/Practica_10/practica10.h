/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 03-02-2022
 * Última modificación: 16-02-2022
 *
 * Descripción: Biblioteca practica0.h para la declaración
 * de funciones relativas a los ejercicios de las prácti-
 * cas.
 *
 * Últimos cambios: Nuevos menús de navegación para ejer-
 * cicios complejos (menupi y menups) y delimitadores para
 * la entrada y salida de datos, además de la finalización
 * de ciertos procesos.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "nav.h"

#ifndef _PRACTICA10_H_
#define _PRACTICA10_H_

int fibonacci_recursivo(int numero);
int fibonacci_iterativo(int numero);
int factorial_recursivo(int numero);
int factorial_iterativo(int numero);
int potencia(int base, int exponente);
int sumar_digitos (int digito);
void tipo_numero(int bandera);
void Hanoi (int n1, int n2, int n3, int n4);

void ejercicio1();
void ejercicio3();

#endif