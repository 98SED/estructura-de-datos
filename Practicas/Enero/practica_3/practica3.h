#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "general.h"

#ifndef _PRACTICA3_H_
#define _PRACTICA3_H_

struct producto
{

    int codigo;
    char descripcion[41];
    float precio;
};

typedef struct
{

    char fabricante[20];
    int cantidad;
    float precio_unitario;

} registro_resistencias;

typedef struct
{

    double x;
    double y;

} coord;

struct existencias
{

    int discos;
    int cintas;
    int cd;
};

struct proveedores
{

    char nombre[40];
    char telefono[10];
    char direccion[100];
};

struct inventario
{

    char titulo[30];
    char autor[40];
    struct existencias inv_existencias;
    struct proveedores inv_proveedores;
};

void ejercicio1_1();
void ejercicio1_2();
void ejercicio2();
void ejercicio3();

#endif