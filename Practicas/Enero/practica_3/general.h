#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifndef _GENERAL_H_
#define _GENERAL_H_

void bienvenidag(char arg_fecha[10], char arg_actividad[13]);
void bienvenidap1(char arg_programa[14], char arg_descln1[44]);
void bienvenidap2(char arg_programa[14], char arg_descln1[44], char arg_descln2[57]);
void bienvenidap3(char arg_programa[14], char arg_descln1[44], char arg_descln2[57], char arg_descln3[57]);
void bienvenidap4(char arg_programa[14], char arg_descln1[44], char arg_descln2[57], char arg_descln3[57], char arg_descln4[57]);
void entrada();
// void lineaes(char arg_linea[58], void *ptr_dirección, char arg_tipo[8]);
// void lineaeg(char arg_linea[58], char *ptr_linea[100]);
void salida();
void menui(int *ptr_opt);
void menus(int *ptr_opt);
void menupi(int *ptr_opt);
void menups(int *ptr_opt);

#endif