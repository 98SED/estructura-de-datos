#include "practica3.h"

void ejercicio1_1()
{

    // Declaración de estructura:
    struct producto producto1, producto2;

    // Descripción del programa:
    bienvenidap3("Ejercicio 01.01", "El presente programa compara el precio de   ",
                 "dos productos ingresados por el usuario y determina cual ",
                 "de los dos es más caro.                                  ");

    // Entrada de datos:
    // Producto 1
    // Código:
    entrada();
    printf(" ║ Ingrese el código del producto 1: ");
    scanf("%d", &producto1.codigo);
    fflush(stdin);

    // Descripción:
    printf(" ║ Añada una descripción del producto %d: ", producto1.codigo);
    getchar();
    fgets(producto1.descripcion, 41, stdin);

    // Precio:
    printf(" ║ Ingrese el precio del producto %d: ", producto1.codigo);
    scanf("%f", &producto1.precio);

    // Producto 2
    // Código:
    printf(" ║ Ingrese el código del producto 2: ");
    scanf("%d", &producto2.codigo);
    fflush(stdin);

    // Descripción:
    printf(" ║ Añada una descripción del producto %d: ", producto2.codigo);
    getchar();
    fgets(producto2.descripcion, 41, stdin);

    // Precio:
    printf(" ║ Ingrese el precio del producto %d: ", producto2.codigo);
    scanf("%f", &producto2.precio);
    salida();

    // Proceso y salida
    if (producto1.precio > producto2.precio)
    {

        printf(" ║ El producto 1 tiene un precio mayor que el producto 2.    ║\n");
    }
    else if (producto2.precio > producto1.precio)
    {

        printf(" ║ El producto 2 tiene un precio mayor que el producto 1.    ║\n");
    }
    else
    {

        printf(" ║ El producto 1 y el producto 2 tienen el mismo precio.     ║\n");
    }

    return;
}

void ejercicio1_2()
{

    registro_resistencias resistencias;

    bienvenidap3(" Ejercicio 1.2 ", "En el presente programa se pone en practica ",
                 "el uso de la palabra reservada typedef por medio de un   ",
                 "programa de inventarios para una refaccionaria.          ");

    entrada();
    printf(" ║ Ingrese un nuevo fabricante de resistencias: ");
    getchar();
    fgets(resistencias.fabricante, 20, stdin);
    printf(" ║ Ingrese la cantidad de resistencias\n");
    printf(" ║ disponibles del fabricante %s: ", resistencias.fabricante);
    scanf("%d", &resistencias.cantidad);
    printf(" ║ Ingrese el precio unitario de las %d\n", resistencias.cantidad);
    printf(" ║ resistencias del fabricante %s: ", resistencias.fabricante);
    scanf("%f", &resistencias.precio_unitario);

    salida();
    printf(" ║ ¡REGISTRO EXITOSO!                                        ║\n");
    printf(" ║ Fabricante: %s", resistencias.fabricante);
    printf(" ║ Cantidad: %d [unidades]\n", resistencias.cantidad);
    printf(" ║ Precio: $%f mxn\n", resistencias.precio_unitario);

    return;
}

void ejercicio2()
{

    coord punto1, punto2;

    bienvenidap3("  Ejercicio 2  ", "El programa permite encontrar la distancia  ",
                 "entre dos puntos a partir de las coordenadas de los      ",
                 "puntos en cuestión.                                      ");

    entrada();
    // lineaes("Ingrese la coordenada x del punto 1: ", &punto1.x, "double");
    printf(" ║ Ingrese la coordenada x del punto 1: ");
    scanf("%lf", &punto1.x);
    printf(" ║ Ingrese la coordenada y del punto 1: ");
    scanf("%lf", &punto1.y);
    printf(" ║ \n");
    printf(" ║ \n");
    printf(" ║ Ingrese la coordenada x del punto 2: ");
    scanf("%lf", &punto2.x);
    printf(" ║ Ingrese la coordenada y del punto 2: ");
    scanf("%lf", &punto2.y);

    salida();
    printf(" ║ La distancia entre los puntos es: %lf \n", sqrt(pow((punto2.x - punto1.x), 2) + pow((punto2.y - punto1.y), 2)));

    return;
}

void ejercicio3()
{

    int opt;
    int count = 5;
    int id;
    struct inventario articulo[5];

    bienvenidap1("  Ejercicio 3  ", "El presente programa muestra el uso de estructuras indentadas ");
    entrada();

    for (int i = 0; i < count; i++)
    {
        printf(" ║ Ingrese el título del articulo %d: ", i);
        getchar();
        fgets(articulo[i].titulo, 30, stdin);
        fflush(stdin);
        printf(" ║ Ingrese el autor del articulo %d: ", i);
        getchar();
        fgets(articulo[i].autor, 40, stdin);
        fflush(stdin);
        printf(" ║ Establezca las existencias del articulo %d: \n", i);
        printf(" ║ Discos: ");
        scanf("%d", &articulo[i].inv_existencias.discos);
        fflush(stdin);
        printf(" ║ Cintas: ");
        scanf("%d", &articulo[i].inv_existencias.cintas);
        fflush(stdin);
        printf(" ║ CD's: ");
        scanf("%d", &articulo[i].inv_existencias.cd);
        fflush(stdin);
        printf(" ║ Ingrese los datos del proveedor del articulo %d: \n", i);
        printf(" ║ Nombre: ");
        getchar();
        fgets(articulo[i].inv_proveedores.nombre, 40, stdin);
        fflush(stdin);
        printf(" ║ Teléfono: ");
        getchar();
        fgets(articulo[i].inv_proveedores.telefono, 10, stdin);
        fflush(stdin);
        printf(" ║ Dirección: ");
        getchar();
        fgets(articulo[i].inv_proveedores.direccion, 100, stdin);
        fflush(stdin);
    }

    do
    {

        menup(&opt);

        switch (opt)
        {
        case 1:

            do
            {
                printf(" ║ Ingrese un código de producto valido para ver sus         ╝\n");
                printf(" ║ existencias: ");
                scanf("%d", &id);
                printf(" ║ El articulo %d cuenta con los siguientes registros:\n", id);
                printf(" ║ Titulo: %s\n", articulo[id].titulo);
                printf(" ║ Autor: %s\n", articulo[id].autor);
                printf(" ║ Existencias: \n");
                printf(" ║ Discos: %d\n", articulo[id].inv_existencias.discos);
                printf(" ║ Cintas: %d\n", articulo[id].inv_existencias.cintas);
                printf(" ║ CD's: %d\n", articulo[id].inv_existencias.cd);
                menup(&opt);
            } while (opt != 5);

            break;
        case 2:

            do
            {
                printf(" ║ Ingrese un código de producto valido para ver su         ╝\n");
                printf(" ║ proveedor: ");
                scanf("%d", &id);
                printf(" ║ El articulo %d cuenta con los siguientes registros:\n", id);
                printf(" ║ Titulo: %s\n", articulo[id].titulo);
                printf(" ║ Autor: %s\n", articulo[id].autor);
                printf(" ║ Proveedor: ");
                printf(" ║ Nombre: %s", articulo[id].inv_proveedores.nombre);
                printf(" ║ Teléfono: %s", articulo[id].inv_proveedores.telefono);
                printf(" ║ Dirección: %s", articulo[id].inv_proveedores.direccion);
                menup(&opt);
            } while (opt != 5);

            break;
        case 3:

            do
            {
                printf(" ║ Ingrese un código de producto valido para ver su         ╝\n");
                printf(" ║ inventario: ");
                scanf("%d", &id);
                printf(" ║ El articulo %d cuenta con los siguientes registros:\n", id);
                printf(" ║ Titulo: %s\n", articulo[id].titulo);
                printf(" ║ Autor: %s\n", articulo[id].autor);
                printf(" ║ Existencias: ");
                printf(" ║ Discos: %d", articulo[id].inv_existencias.discos);
                printf(" ║ Cintas: %d", articulo[id].inv_existencias.cintas);
                printf(" ║ CD's: %d", articulo[id].inv_existencias.cd);
                printf(" ║ Proveedor: ");
                printf(" ║ Nombre: %s", articulo[id].inv_proveedores.nombre);
                printf(" ║ Teléfono: %s", articulo[id].inv_proveedores.telefono);
                printf(" ║ Dirección: %s", articulo[id].inv_proveedores.direccion);
                menup(&opt);
            } while (opt != 5);

            break;
        case 4:
            printf(" ║ Opción invalida en la sección actual");
            break;
        case 5:
            printf(" ║ Opción invalida en la sección actual");
            break;
        case 6:
            opt = 0;
            break;

        default:
            printf("Ingresa una opción valida");
            break;
        }

    } while (opt != 0);

    return;
}