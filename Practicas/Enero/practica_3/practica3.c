#include "practica3.h"

void menu();

int main()
{

    int opt = 404;

    do
    {
        if (opt == 404)
        {
            bienvenidag("01-02-2022", "Practica 03");
            menui(&opt);
        }
        else
        {
            menus(&opt);
        }
        switch (opt)
        {
        case 1:
            ejercicio1_1();
            break;
        case 2:
            ejercicio1_2();
            break;
        case 3:
            ejercicio2();
            break;
        case 4:
            ejercicio3();
            break;
        case 5:
            printf(" ╚═══════════════════════════════════════════════════════════╝\n");
            opt = 0;
            break;

        default:
            printf("Ingresa una opción valida");
            break;
        }

    } while (opt != 0);

    return 0;
}