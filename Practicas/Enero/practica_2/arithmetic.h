#ifndef _ARITHMETIC_H_
#define _ARITHMETIC_H_

double add(double arg_n1, double arg_n2);
double sub(double arg_n1, double arg_n2);
double mul(double arg_n1, double arg_n2);
double div(double arg_n1, double arg_n2);


#endif