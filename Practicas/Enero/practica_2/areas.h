#include <math.h>

#ifndef _AREAS_H_
#define _AREAS_H_

double squarea(double arg_length);
double cirarea(double arg_radius);
double triarea(double arg_base, double arg_height);
double recarea(double arg_length, double arg_width);
double traarea(double arg_lengtha, double arg_lengthb, double arg_height);
double ellarea(double arg_minaxis, double arg_maxaxis);

void bienvenida();

#endif