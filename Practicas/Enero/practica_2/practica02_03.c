/*
 * DESCRIPCIÓN DEL PROGRAMA:
 * En el presente programa se muestra el funcionamiento de la función strcat()
 * de la biblioteca string.h
 *
 * Alumno programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 25-01-2022
 * Actividad: practica02_03.c
 */

// Bibliotecas
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main()
{

    // Declaración de variables:
    char ch1 = 'R';
    char ch2 = 'r';
    char ch3 = '4';
    char ch4 = ' ';


    // Ejecución de funciones:
    printf("Caracter 1: %c\n", ch1);
    printf("Caracter 2: %c\n", ch2);
    printf("Caracter 3: %c\n", ch3);
    printf("Caracter 4: %c\n\n", ch4);

    printf("isalnum(): \n");
    printf("Caracter 1: %d\n", isalnum(ch1));
    printf("Caracter 2: %d\n", isalnum(ch2));
    printf("Caracter 3: %d\n", isalnum(ch3));
    printf("Caracter 4: %d\n", isalnum(ch4));

    printf("\n\nisalpha(): \n");
    printf("Caracter 1: %d\n", isalpha(ch1));
    printf("Caracter 2: %d\n", isalpha(ch2));
    printf("Caracter 3: %d\n", isalpha(ch3));
    printf("Caracter 4: %d\n", isalpha(ch4));

    printf("\n\nisblank(): \n");
    printf("Caracter 1: %d\n", isblank(ch1));
    printf("Caracter 2: %d\n", isblank(ch2));
    printf("Caracter 3: %d\n", isblank(ch3));
    printf("Caracter 4: %d\n", isblank(ch4));

    printf("\n\nisdigit(): \n");
    printf("Caracter 1: %d\n", isdigit(ch1));
    printf("Caracter 2: %d\n", isdigit(ch2));
    printf("Caracter 3: %d\n", isdigit(ch3));
    printf("Caracter 4: %d\n", isdigit(ch4));

    printf("\n\nisspace(): \n");
    printf("Caracter 1: %d\n", isspace(ch1));
    printf("Caracter 2: %d\n", isspace(ch2));
    printf("Caracter 3: %d\n", isspace(ch3));
    printf("Caracter 4: %d\n", isspace(ch4));

    printf("\n\nislower(): \n");
    printf("Caracter 1: %d\n", islower(ch1));
    printf("Caracter 2: %d\n", islower(ch2));
    printf("Caracter 3: %d\n", islower(ch3));
    printf("Caracter 4: %d\n", islower(ch4));

    printf("\n\nisupper(): \n");
    printf("Caracter 1: %d\n", isupper(ch1));
    printf("Caracter 2: %d\n", isupper(ch2));
    printf("Caracter 3: %d\n", isupper(ch3));
    printf("Caracter 4: %d\n", isupper(ch4));

    printf("\n\ntolower(): \n");
    printf("Caracter 1: %c\n", tolower(ch1));
    printf("Caracter 2: %c\n", tolower(ch2));

    printf("\n\ntoupper(): \n");
    printf("Caracter 1: %c\n", toupper(ch1));
    printf("Caracter 2: %c\n", toupper(ch2));



    return 0;

}