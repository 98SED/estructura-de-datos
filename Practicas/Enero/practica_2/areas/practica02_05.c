/*
 * DESCRIPCIÓN DEL PROGRAMA:
 * En el presente programa se pone en practica la creación de programas
 * utilizando multiples archivos, tanto bibliotecas (.h) como archivos
 * de código fuente auxiliares (.c) para realizar el calculo de areas.
 *
 * Alumno programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 25-01-2022
 * Actividad: practica02_05.c
 */

#include <stdio.h>
#include "areas.h"



int main()
{

    // Declaración de variables:
    double circulo = 0;
    double radio = 2;

    // Ejecución de funciones:
    circulo = cirarea(radio);
    printf("%lf", circulo);

    return 0;

}

