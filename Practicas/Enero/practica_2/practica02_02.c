/*
 * DESCRIPCIÓN DEL PROGRAMA:
 * En el presente programa se muestra el funcionamiento de la función strlen()
 * de la biblioteca string.h
 *
 * Alumno programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 25-01-2022
 * Actividad: practica02_02.c
 */

#include <stdio.h>
#include <string.h>
#define MAXLON 80
#ifdef _WIN32
#include <conio.h>
#else
#include <stdio.h>
#define clrscr() printf("\e[1;1H\e[2J")
#endif


int main()
{

    // Declaración de variables:
    char a[MAXLON+1];
    int longitud;

    // Ejecución de funciones:
    clrscr();
    printf("Introduce una cadena (max. %d caracteres): ", MAXLON);
    scanf("%s", &a);
    longitud = strlen(a);
    printf("\nLongitud de la cadena: %d\n", longitud);

    return 0;

}