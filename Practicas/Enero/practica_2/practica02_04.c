/*
 * DESCRIPCIÓN DEL PROGRAMA:
 * En el presente programa se pone en practica la creación de programas
 * utilizando multiples archivos, tanto bibliotecas (.h) como archivos
 * de código fuente auxiliares (.c) para realizar el calculo de areas.
 *
 * Alumno programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 25-01-2022
 * Actividad: practica02_04.c
 */

#include <stdio.h>
#include "arithmetic.h"

int main()
{

    // Declaración de variables:
    double n1 = 0.0;
    double n2 = 0.0;

    // Ejecución de funciones:
    printf("Ingrese el número 1: ");
    scanf("%lf", &n1);
    printf("Ingrese el número 2: ");
    scanf("%lf", &n2);
    printf("\n\n Número 1: %lf, Número 2: %lf", n1, n2);
    printf("\n\n Suma: %lf\n", add(n1,n2));
    printf(" Resta: %lf\n", sub(n1,n2));
    printf(" Multiplicación: %lf\n", mul(n1,n2));
    printf(" División: %lf\n", div(n1,n2));

    return 0;

}