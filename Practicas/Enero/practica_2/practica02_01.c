/*
 * DESCRIPCIÓN DEL PROGRAMA:
 * En el presente programa se muestra el funcionamiento de la función strcat()
 * de la biblioteca string.h
 *
 * Alumno programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 25-01-2022
 * Actividad: practica02_01.c
 */

// Bibliotecas
#include <stdio.h>
#include <string.h>

int main()
{

    // Declaración de variables:
    char texto1[] = "Don Pepito";
    char texto2[] = " y ";
    char texto3[] = "Don Jose";

    // Ejecución de funciones:
    printf("%s\n", texto1);

    strcat(texto1, texto2);
    printf("%s\n", texto1);
    strcat(texto1, texto3);
    printf("%s\n", texto1);
    getchar();

    return 0;

}
