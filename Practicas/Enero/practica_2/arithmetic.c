#include "arithmetic.h"

double add(double arg_n1, double arg_n2)
{

    return arg_n1 + arg_n2;

}

double sub(double arg_n1, double arg_n2)
{

    return arg_n1 - arg_n2;

}

double mul(double arg_n1, double arg_n2)
{

    return arg_n1 * arg_n2;

}

double div(double arg_n1, double arg_n2)
{

    return arg_n1 / arg_n2;

}
