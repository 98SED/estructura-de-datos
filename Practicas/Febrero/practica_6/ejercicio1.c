/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 00-00-2022
 *
 * Descripción:
 *
 *
 */

#include "practica6.h"

void ejercicio1()
{

    // Declaraciones
    float **matriz;
    int filas, columnas;



    // Mensaje inicial al usuario
    bienvprog3("  Ejercicio 1  ", "El presente programa se encarga de reservar ", "memoria para una matriz de dos dimensiones utilizando    ", "memoria dinámica y arreglos de apuntadores."); /* 15, 44, 57 */

    // Entrada
    msjent();
    matbuild(&filas, &columnas, &matriz);

    // Proceso y salida
    msjsal();
    printf(" ║ ========================================================= ║\n");
    printf(" ║                                                           ║\n");
    printf(" ║                  La matriz ingresada es:                  ║\n");
    printf(" ║                                                           ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ \n");
    matprint(&filas, &columnas, matriz);

    msjfinprog();
}

/**
 * printf(" ║                                                           ║\n");
 *
 * printf(" ╠═══════════════════════════════════════════════════════════╣\n");
 *
 * printf(" ║ ========================================================= ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║ ========================================================= ║\n");
 *
 * printf(" ║ % ║ \n", var);
 *
 * printf(" ║ % \n", var);
 *
 * printf(" ║ \n");
 *
 * printf(" ║ Ingresa una opción: ");
 * scanf("%", var);
 *
 * printf(" ║ Ingresa una opción: ");
 * fgets(var, 0, stdin);
 */