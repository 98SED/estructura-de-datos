#include "matrix.h"

void matbuild(int *ptr_fil, int *ptr_col, float ***ptr_matrix)
{

    float **matrix;
    int i, j, k;

    printf(" ║ Ingresa el número de filas de la matriz: ");
    scanf("%d", ptr_fil);
    printf(" ║ \n");
    printf(" ║ Ingresa el número de columnas de matriz: ");
    scanf("%d", ptr_col);
    printf(" ║ \n");

    matrix = (float **)calloc(*ptr_fil, sizeof(float));
    if (!matrix)
        return NULL;
    for (i = 0; i < *ptr_fil; i++)
    {
        matrix[i] = (float *)calloc(*ptr_col, sizeof(float));
        if (!matrix[i])
            return NULL;
    }

    for (i = 0; i < *ptr_fil; i++)
    {
        for (j = 0; j < *ptr_col; j++)
        {
            printf(" ║ Ingresa el valor de la posición (%d,%d): ", i + 1, j + 1);
            scanf("%f", &matrix[i][j]);
        }
        printf(" ║ \n");
    }
    *ptr_matrix = matrix;
}

void matprint(int *ptr_fil, int *ptr_col, float **ptr_matrix)
{

    for (int i = 0; i < *ptr_fil; i++)
    {

        printf(" ║ |");
        for (int j = 0; j < *ptr_col; j++)
        {
            printf(" %lf", ptr_matrix[i][j]);
        }
        printf(" |\n");

    }

    printf(" ║ \n");

}