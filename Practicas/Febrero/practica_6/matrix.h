#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "general.h"

#ifndef _MATRIX_H_
#define _MATRIX_H_

void matbuild(int *ptr_fil, int *ptr_col, float ***ptr_matrix);
void matprint(int *ptr_fil, int *ptr_col, float **ptr_matrix);

#endif