/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 00-00-2022
 *
 * Descripción: 
 * 
 * 
 */

#include "practica6.h"

void ejercicio3()
{

    // Declaraciones
    float **matriz;
    int filas, columnas, i, j;
    float acumulador;

    // Mensaje inicial al usuario
    bienvprog1("  Ejercicio 3  ", ""); /* 15, 44, 57 */
    printf(" ║ SIN TERMINAR\n");

    // Entrada
    msjent();
    matbuild(&filas, &columnas, &matriz);


    // Proceso y salida
    msjsal();
    for (i = 0; i < filas; i++)
    {
        for (j = 0; j < columnas; j++)
        {
            acumulador += matriz[i][j];
        }
        
    }
    
    matprint(&filas, &columnas, matriz);
    printf(" ║ La suma de las ventas es: %lf\n", acumulador);


    msjfinprog();


}

/**
 * printf(" ║                                                           ║\n");
 *
 * printf(" ╠═══════════════════════════════════════════════════════════╣\n");
 *
 * printf(" ║ ========================================================= ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║ ========================================================= ║\n");
 *
 * printf(" ║ % ║ \n", var);
 *
 * printf(" ║ % \n", var);
 *
 * printf(" ║ \n");
 *
 * printf(" ║ Ingresa una opción: ");
 * scanf("%", var);
 *
 * printf(" ║ Ingresa una opción: ");
 * fgets(var, 0, stdin);
 */