/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 00-00-2022
 *
 * Descripción: 
 * 
 * 
 */

#include "practica5.h"

void ejercicio5()
{

    // Declaraciones


    // Mensaje inicial al usuario
    bienvprog1("  Ejercicio 5 ", "");

    // Entrada
    msjent();


    // Proceso y salida
    msjsal();


    msjfinprog();


}