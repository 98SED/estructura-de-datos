/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 00-00-2022
 *
 * Descripción:
 *
 *
 */

#include "practica5.h"

void ejercicio1()
{

    // Declaraciones
    struct punto punto1, *ptr_punto = &punto1;
    int opt;

    // Reasignaciones
    punto1.x = 0;
    punto1.y = 0;

    // Mensaje inicial al usuario
    bienvprog1("  Ejercicio 1  ", "");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║                                                           ║\n");
    printf(" ║ Valores iniciales y dirección de memoria de apuntadores:  ║\n");
    printf(" ║                                                           ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ====================== Variable (x) ===================== ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ Valor almacenado: %d                                       ║\n", punto1.x);
    printf(" ║ Dirección de memoria: %p                      ║\n", &punto1.x);
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ====================== Variable (y) ===================== ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ Valor almacenado: %d                                       ║\n", punto1.y);
    printf(" ║ Dirección de memoria: %p                      ║\n", &punto1.y);
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ================= Apuntador (ptr_punto) ================= ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ Valor apuntado (x,y): (%d, %d)                              ║\n", ptr_punto->x, ptr_punto->y);
    printf(" ║ Dirección: %p                                 ║\n", &ptr_punto);
    printf(" ║ Dirección de memoria apuntada:                            ║\n");
    printf(" ║ Estructura: %p                                ║\n", ptr_punto); // ? Las estructuras ocupan espacio en la memoria (tienen una dirección de memoria)?
    printf(" ║ x: %p                                         ║\n", &ptr_punto->x);
    printf(" ║ y: %p                                         ║\n", &ptr_punto->y);

    // Procesos

    do
    {
        menprog("  1 - Reasignar  ", "   2 - Regresar  ", "                 ", &opt);

        switch (opt)
        {
        case 1:
            printf(" ║ ========================================================= ║\n");
            printf(" ║                                                           ║\n");
            printf(" ║ Reasignación de variable por medio de apuntadores:        ║\n");
            printf(" ║                                                           ║\n");
            printf(" ║ ========================================================= ║\n");

            // Entrada
            msjent();

            printf(" ║ \n");
            printf(" ║ Ingrese un valor de x: ");
            scanf("%d", &ptr_punto->x);
            printf(" ║ \n");
            printf(" ║ Ingrese un valor de y: ");
            scanf("%d", &ptr_punto->y);
            printf(" ║ \n");

            // Proceso y salida
            msjsal();

            printf(" ║ ========================================================= ║\n");
            printf(" ║ ====================== Variable (x) ===================== ║\n");
            printf(" ║ ========================================================= ║\n");
            printf(" ║ Valor almacenado: %d                                       ║\n", punto1.x);
            printf(" ║ Dirección de memoria: %p                      ║\n", &punto1.x);
            printf(" ║ ========================================================= ║\n");
            printf(" ║ ====================== Variable (y) ===================== ║\n");
            printf(" ║ ========================================================= ║\n");
            printf(" ║ Valor almacenado: %d                                       ║\n", punto1.y);
            printf(" ║ Dirección de memoria: %p                      ║\n", &punto1.y);
            printf(" ║ ========================================================= ║\n");
            printf(" ║ ================= Apuntador (ptr_punto) ================= ║\n");
            printf(" ║ ========================================================= ║\n");
            printf(" ║ Valor apuntado (x,y): (%d, %d)                              ║\n", ptr_punto->x, ptr_punto->y);
            printf(" ║ Dirección: %p                                 ║\n", &ptr_punto);
            printf(" ║ Dirección de memoria apuntada:                            ║\n");
            printf(" ║ Estructura: %p                                ║\n", ptr_punto); // ? Las estructuras ocupan espacio en la memoria (tienen una dirección de memoria)?
            printf(" ║ x: %p                                         ║\n", &ptr_punto->x);
            printf(" ║ y: %p                                         ║\n", &ptr_punto->y);
            break;

        case 2:
            break;

        default:
            msjerr("             La opción ingresada no es valida            ", "               Ingresa una opción valida...              ");
            break;
        }

    } while (opt != 2);

    msjfinprog();
}