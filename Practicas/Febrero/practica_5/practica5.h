/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 03-02-2022
 * Última modificación: 16-02-2022
 *
 * Descripción: Biblioteca practica0.h para la declaración
 * de funciones relativas a los ejercicios de las prácti-
 * cas.
 *
 * Últimos cambios: Nuevos menús de navegación para ejer-
 * cicios complejos (menupi y menups) y delimitadores para
 * la entrada y salida de datos, además de la finalización
 * de ciertos procesos.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "general.h"

#ifndef _PRACTICA5_H_
#define _PRACTICA5_H_

// Declaración de Estructuras
struct punto
{
    int x;
    int y;
};

typedef struct
{
    char calle[30];
    int numero;
    int cp;
    char ciudad[30];
} dir;

typedef struct
{
    char nombre[30];
    int matricula;
    dir domicilio;
} alumno;

typedef struct
{
    int num;
    char car;
}tiporegistro;

// Declaración de Funciones
void ejercicio1();
void ejercicio2();
void ejercicio3();
void ejercicio4();
void ejercicio5();

#endif