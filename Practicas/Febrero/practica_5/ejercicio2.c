/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 00-00-2022
 *
 * Descripción:
 *
 *
 */

#include "practica5.h"

void ejercicio2()
{

    // Declaraciones
    int opt;
    alumno alumno1, *ptr_alumno1 = &alumno1;

    // Mensaje inicial al usuario
    bienvprog6("  Ejercicio 2  ", "En el presente programa se pone en practica ",
               "el uso de apuntadores con estrucutras anidadas para ana- ",
               "lizar el comportamiento de la memoria y almacenamiento de",
               "estos.                                                   ",
               "A continuación se solicitarán datos de prueba para una   ",
               "estructura relacionada con información de alumnos...     ");

    // Entrada
    msjent();

    printf(" ║ \n");
    printf(" ║ Ingrese el nombre del alumno: ");
    getchar();
    fgets(ptr_alumno1->nombre, 30, stdin);
    printf(" ║ \n");
    printf(" ║ Ingrese la matricula del alumno: ");
    scanf("%d", &ptr_alumno1->matricula);
    printf(" ║ \n");
    printf(" ║ Ingrese la dirección del alumno: \n");
    printf(" ║ \n");
    printf(" ║ Calle: ");
    getchar();
    fgets(ptr_alumno1->domicilio.calle, 30, stdin);
    printf(" ║ Número: ");
    scanf("%d", &ptr_alumno1->domicilio.numero);
    printf(" ║ Ciudad: ");
    getchar();
    fgets(ptr_alumno1->domicilio.ciudad, 30, stdin);
    printf(" ║ Código Postal: ");
    scanf("%d", &ptr_alumno1->domicilio.cp);
    printf(" ║ \n");

    // Proceso y salida

    do
    {
        menprog(" 1 - Consl. Regs.", " 2 - Mod. Alum1. ", "   3 - Regresar  ", &opt);

        switch (opt)
        {
        case 1:

            msjent();

            printf(" ║ ========================================================= ║\n");
            printf(" ║                                                           ║\n");
            printf(" ║ Consulta de registros Almacenados:                        ║\n");
            printf(" ║                                                           ║\n");
            printf(" ║ ========================================================= ║\n");

            // Proceso y salida
            msjsal();

            printf(" ║ ========================================================= ║\n");
            printf(" ║ ======================== Alumno 1 ======================= ║\n");
            printf(" ║ ========================================================= ║\n");
            printf(" ║ Nombre: %s", ptr_alumno1->nombre);
            printf(" ║ Matricula: %d\n", ptr_alumno1->matricula);
            printf(" ║ \n");
            printf(" ║ Domicilio:\n");
            printf(" ║ Calle: %s", ptr_alumno1->domicilio.calle);
            printf(" ║ Número: %d\n", ptr_alumno1->domicilio.numero);
            printf(" ║ Ciudad: %s", ptr_alumno1->domicilio.ciudad);
            printf(" ║ Código Postal: %d\n", ptr_alumno1->domicilio.cp);
            break;

        case 2:

            printf(" ║ \n");
            printf(" ║ Ingrese el nombre del alumno: ");
            getchar();
            fgets(ptr_alumno1->nombre, 30, stdin);
            printf(" ║ \n");
            printf(" ║ Ingrese la matricula del alumno: ");
            scanf("%d", &ptr_alumno1->matricula);
            printf(" ║ \n");
            printf(" ║ Ingrese la dirección del alumno: \n");
            printf(" ║ \n");
            printf(" ║ Calle: ");
            getchar();
            fgets(ptr_alumno1->domicilio.calle, 30, stdin);
            printf(" ║ Número: ");
            scanf("%d", &ptr_alumno1->domicilio.numero);
            printf(" ║ Ciudad: ");
            getchar();
            fgets(ptr_alumno1->domicilio.ciudad, 30, stdin);
            printf(" ║ Código Postal: ");
            scanf("%d", &ptr_alumno1->domicilio.cp);
            printf(" ║ \n");

            break;

        case 3:
            break;

        default:
            msjerr("             La opción ingresada no es valida            ", "               Ingresa una opción valida...              ");
            break;
        }

    } while (opt != 3);

    msjfinprog();
}

/**
 * printf(" ║                                                           ║\n");
 *
 * printf(" ╠═══════════════════════════════════════════════════════════╣\n");
 *
 * printf(" ║ ========================================================= ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║ ========================================================= ║\n");
 *
 * printf(" ║ % ║\n", var);
 *
 * printf(" ║ % \n", var);
 *
 * printf(" ║ \n");
 *
 * printf(" ║ Ingresa una opción: ");
 * scanf("%", var);
 */