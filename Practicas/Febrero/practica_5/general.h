/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 03-02-2022
 * Última modificación: 16-02-2022
 *
 * Descripción: Biblioteca general.h para menús de navega-
 * ción CUI para emuladores de terminal.
 *
 * Últimos cambios: Nuevos menús de navegación para ejer-
 * cicios complejos (menupi y menups) y delimitadores para
 * la entrada y salida de datos, además de la finalización
 * de ciertos procesos.
 */

#include <stdio.h>
#include <string.h>

#ifndef _GENERAL_H_
#define _GENERAL_H_

// Mensajes de bienvenida
void bienv(char str_fecha[11], char str_actividad[12]);
void bienvprog1(char str_programa[15], char str_descln1[45]);
void bienvprog2(char str_programa[15], char str_descln1[45], char str_descln2[58]);
void bienvprog3(char str_programa[15], char str_descln1[45], char str_descln2[58], char str_descln3[58]);
void bienvprog4(char str_programa[15], char str_descln1[45], char str_descln2[58], char str_descln3[58], char str_descln4[58]);
void bienvprog5(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58], char str_descln4[58], char str_descln5[58]);
void bienvprog6(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58], char str_descln4[58], char str_descln5[58], char str_descln6[58]);

// Mensajes de procesos
void msjent();
void msjsal();
void msjfin();
void msjfinprog();
void msjerr();

// Menus de navegación
void menpri(char str_cas1[18], char str_cas2[18], char str_cas3[18], char str_cas4[18], char str_cas5[18], int *ptr_opt);
void menprog(char str_cas1[18], char str_cas2[18], char str_cas3[18], int *ptr_opt);

#endif