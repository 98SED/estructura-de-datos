/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 03-02-2022
 * Última modificación: 16-02-2022
 *
 * Descripción: Archivo auxiliar de la biblioteca general.h
 * con definiciones de funciones relativas a menus de nave-
 * gación CUI para emuladores de terminal.
 */

#include "general.h"

// ! Biblio string strcpy memoria dinamica

void bienv(char str_fecha[11], char str_actividad[12])
{

    printf("\n");
    printf(" ╔═══════════════════════════════════════════════════════════╗\n");
    printf(" ║ Alumno Programador: Rodrigo Eduardo Delgadillo González   ║\n");
    printf(" ║ Fecha: %s                                         ║\n", str_fecha);
    printf(" ║ Actividad: %s                                    ║\n", str_actividad);
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ¡Bienvenido Usuario! Introduce una de las siguientes      ║\n");
    printf(" ║ opciones para ejecutar un programa:                       ║\n");
    printf(" ╠═══════════════════╦═══════════════════╦═══════════════════╣\n");
}

void bienvprog1(char str_programa[16], char str_descln1[45])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", str_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", str_descln1);

}

void bienvprog2(char str_programa[16], char str_descln1[45], char str_descln2[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", str_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", str_descln1);
    printf(" ║ %s ║\n", str_descln2);

}

void bienvprog3(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", str_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", str_descln1);
    printf(" ║ %s ║\n", str_descln2);
    printf(" ║ %s ║\n", str_descln3);

}

void bienvprog4(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58], char str_descln4[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", str_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", str_descln1);
    printf(" ║ %s ║\n", str_descln2);
    printf(" ║ %s ║\n", str_descln3);
    printf(" ║ %s ║\n", str_descln4);

}

void bienvprog5(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58], char str_descln4[58], char str_descln5[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", str_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", str_descln1);
    printf(" ║ %s ║\n", str_descln2);
    printf(" ║ %s ║\n", str_descln3);
    printf(" ║ %s ║\n", str_descln4);
    printf(" ║                                                           ║\n");
    printf(" ║ %s ║\n", str_descln5);

}

void bienvprog6(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58], char str_descln4[58], char str_descln5[58], char str_descln6[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", str_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", str_descln1);
    printf(" ║ %s ║\n", str_descln2);
    printf(" ║ %s ║\n", str_descln3);
    printf(" ║ %s ║\n", str_descln4);
    printf(" ║                                                           ║\n");
    printf(" ║ %s ║\n", str_descln5);
    printf(" ║ %s ║\n", str_descln6);

}

void msjent()
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ======================== ENTRADA ======================== ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╝\n");
}

void msjsal()
{

    printf(" ╠═══════════════════════════════════════════════════════════╗\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ======================== SALIDA ========================= ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║                                                           ║\n");
}

void msjfin()
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ========================== FIN ========================== ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ La ejecución del programa finalizó, gracias por probarlo. ║\n");
    printf(" ║ Dudas, comentarios y sugerencias: red_g4898@outlook.com   ║\n");
    printf(" ╚═══════════════════════════════════════════════════════════╝\n");
}

void msjfinprog()
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================== FIN ========================== ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ El programa anterior terminó de ejecutarse, selecciona    ║\n");
    printf(" ║ otra opción disponible:                                   ║\n");
    printf(" ╠═══════════════════╦═══════════════════╦═══════════════════╣\n");
}

void msjerr(char causa[58], char solucion[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║===========================================================║\n");
    printf(" ║==========¡ERROR!=========¡ERROR!=========¡ERROR!==========║\n");
    printf(" ║===========================================================║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ %s ║\n", causa);
    printf(" ║ %s ║\n", solucion);
    printf(" ╠═══════════════════╦═══════════════════╦═══════════════════╣\n");
}

void menpri(char str_cas1[18], char str_cas2[18], char str_cas3[18], char str_cas4[18], char str_cas5[18], int *ptr_opt)
{

    printf(" ║ %s ║ %s ║ %s ║\n", str_cas1, str_cas2, str_cas3);
    printf(" ╠═══════════════════╬═══════════════════╬═══════════════════╣\n");
    printf(" ║ %s ║ %s ║     6 - Salir     ║\n", str_cas4, str_cas5);
    printf(" ╠═══════════════════╩═══════════════════╩═══════════════════╝\n");
    printf(" ║ Ingresa una opción: ");
    scanf("%d", ptr_opt);
}

void menprog(char str_cas1[18], char str_cas2[18], char str_cas3[18], int *ptr_opt)
{

    printf(" ╠═══════════════════╦═══════════════════╦═══════════════════╣\n");
    printf(" ║ %s ║ %s ║ %s ║\n", str_cas1, str_cas2, str_cas3);
    printf(" ╠═══════════════════╩═══════════════════╩═══════════════════╝\n");
    printf(" ║ Ingresa una opción: ");
    scanf("%d", ptr_opt);
}