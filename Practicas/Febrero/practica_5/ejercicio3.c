/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 00-00-2022
 *
 * Descripción: 
 * 
 * 
 */

#include "practica5.h"

void ejercicio3()
{

    // Declaraciones
    typedef tiporegistro *tipopuntero;
    tipopuntero ptr;

    // Asignación de memoria
    ptr = (tipopuntero)malloc(sizeof(tiporegistro));

    // Mensaje inicial al usuario
    bienvprog1("  Ejercicio 3 ", "");

    // Entrada
    msjent();

    printf(" ║ \n");
    printf(" ║ Introduce el identificador: ");
    scanf("%i", &ptr->num);
    fflush(stdin);

    printf(" ║ Introduce el caracter: ");
    getchar();
    scanf("%c", &ptr->car);
    printf(" ║ \n");


    // Proceso y salida
    msjsal();

    printf(" ║ Identificador: %d \n", ptr->num);
    printf(" ║ Caracter: %c \n", ptr->car);
    printf(" ║ \n");

    msjfinprog();


}

/**
 * printf(" ║                                                           ║\n");
 *
 * printf(" ╠═══════════════════════════════════════════════════════════╣\n");
 *
 * printf(" ║ ========================================================= ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║                                                           ║\n");
 * printf(" ║ ========================================================= ║\n");
 *
 * printf(" ║ % ║\n", var);
 *
 * printf(" ║ % \n", var);
 *
 * printf(" ║ \n");
 *
 * printf(" ║ Ingresa una opción: ");
 * scanf("%", var);
 *
 * printf(" ║ Ingresa una opción: ");
 * fgets(var, 0, stdin);
 */