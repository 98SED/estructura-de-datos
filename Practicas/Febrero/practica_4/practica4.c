/*
** Alumno Programador: Rodrigo Eduardo Delgadillo González
** Fecha: 16-02-2022
**
** Descripción:
**
**
*/

#include "practica4.h"

int main()
{

    int opt = 404;

    do
    {
        if (opt == 404)
        {
            bienvenidag("16-02-2022", "Practica 4 ");
            menui(&opt);
        }
        else
        {
            menus(&opt);
        }
        switch (opt)
        {
        case 1:
            ejercicio1();
            break;
        case 2:
            ejercicio2();
            break;
        case 3:
            ejercicio3();
            break;
        case 4:
            ejercicio4();
            break;
        case 5:
            //ejercicio5();
            break;
        case 6:
            printf(" ╚═══════════════════════════════════════════════════════════╝\n");
            opt = 0;
            break;

        default:
            printf("Ingresa una opción valida");
            break;
        }

    } while (opt != 0);

    return 0;
}