/*
** Alumno Programador: Rodrigo Eduardo Delgadillo González
** Fecha: 03-02-2022
** Última modificación: 16-02-2022
**
** Descripción: Archivo auxiliar de la biblioteca general.h
** con definiciones de funciones relativas a menus de nave-
** gación CUI para emuladores de terminal.
*/

#include "general.h"

// ! Biblio string strcpy memoria dinamica

void bienvenidag(char arg_fecha[22], char arg_actividad[24])
{

    printf("\n");
    printf(" ╔═══════════════════════════════════════════════════════════╗\n");
    printf(" ║ Alumno Programador: Rodrigo Eduardo Delgadillo González   ║\n");
    printf(" ║ Fecha: %s                                         ║\n", arg_fecha);
    printf(" ║ Actividad: %s                                    ║\n", arg_actividad);
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ¡Bienvenido Usuario! Introduce una de las siguientes      ║\n");
    printf(" ║ opciones para ejecutar un programa:                       ║\n");
    printf(" ╠═══════════════════╦═══════════════════╦═══════════════════╣\n");
}

void bienvenidap1(char arg_programa[16], char arg_descln1[45])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", arg_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", arg_descln1);
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
}

void bienvenidap2(char arg_programa[16], char arg_descln1[45], char arg_descln2[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", arg_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", arg_descln1);
    printf(" ║ %s ║\n", arg_descln2);
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
}

void bienvenidap3(char arg_programa[16], char arg_descln1[45], char arg_descln2[58], char arg_descln3[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", arg_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", arg_descln1);
    printf(" ║ %s ║\n", arg_descln2);
    printf(" ║ %s ║\n", arg_descln3);
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
}

void bienvenidap4(char arg_programa[16], char arg_descln1[45], char arg_descln2[58], char arg_descln3[58], char arg_descln4[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", arg_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", arg_descln1);
    printf(" ║ %s ║\n", arg_descln2);
    printf(" ║ %s ║\n", arg_descln3);
    printf(" ║ %s ║\n", arg_descln4);
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
}

void entrada()
{

    printf(" ║ ========================================================= ║\n");
    printf(" ║ ======================== ENTRADA ======================== ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╝\n");
}

/* void lineaes(char arg_linea[58], void *ptr_dirección, char arg_tipo[8]) {

    printf(" ║ %s", arg_linea);
    if(arg_tipo == "int"){

        scanf("%d", ptr_dirección);
        fflush(stdin);

    }else if(arg_tipo == "float"){

        scanf("%f", ptr_dirección);
        fflush(stdin);

    }else if(arg_tipo == "double"){

        scanf("%lf", ptr_dirección);
        fflush(stdin);

    }

} */

/* void lineaeg(char arg_linea[58], char *ptr_linea[100]) {

    printf(" ║ %s", arg_linea);
    fgets(*ptr_linea, 58, stdin);
    fflush(stdin);

} */

void salida()
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ======================== SALIDA ========================= ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║                                                           ║\n");
}

void menui(int *ptr_opt)
{

    printf(" ║  1 - Ejercicio 1  ║  2 - Ejercicio 2  ║  3 - Ejercicio 3  ║\n");
    printf(" ╠═══════════════════╬═══════════════════╬═══════════════════╣\n");
    printf(" ║  4 - Ejercicio 4  ║  5 - Ejercicio 5  ║     6 - Salir     ║\n");
    printf(" ╠═══════════════════╩═══════════════════╩═══════════════════╝\n");
    printf(" ║ Ingresa una opción: ");
    scanf("%d", ptr_opt);
}

void menus(int *ptr_opt)
{

    printf(" ║                                                           ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================== FIN ========================== ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ El programa anterior terminó de ejecutarse, selecciona    ║\n");
    printf(" ║ otra opción disponible:                                   ║\n");
    printf(" ╠═══════════════════╦═══════════════════╦═══════════════════╣\n");
    printf(" ║  1 - Ejercicio 1  ║  2 - Ejercicio 2  ║  3 - Ejercicio 3  ║\n");
    printf(" ╠═══════════════════╬═══════════════════╬═══════════════════╣\n");
    printf(" ║  4 - Ejercicio 4  ║  5 - Ejercicio 5  ║     6 - Salir     ║\n");
    printf(" ╠═══════════════════╩═══════════════════╩═══════════════════╝\n");
    printf(" ║ Ingresa una opción: ");
    scanf("%d", ptr_opt);
}

// TODO: Corregir

void menup(int *ptr_opt)
{

    printf(" ╠═══════════════════╦═══════════════════╦═══════════════════╣\n");
    printf(" ║   1 - Reasignar   ║   2 - Regresar    ║                   ║\n");
    printf(" ╠═══════════════════╩═══════════════════╩═══════════════════╝\n");
    printf(" ║ Ingresa una opción: ");
    scanf("%d", ptr_opt);
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
}

void menups(int *ptr_opt)
{

    printf(" ║  1 - Existencias  ║  2 - Proveedores  ║  3 - Inventario   ║\n");
    printf(" ╠═══════════════════╬═══════════════════╬═══════════════════╣\n");
    printf(" ║  4 - Nueva Búsqu  ║   5 - Regresar    ║     6 - Salir     ║\n");
    printf(" ╠═══════════════════╩═══════════════════╩═══════════════════╝\n");
    printf(" ║ Ingresa una opción: ");
    scanf("%d", ptr_opt);
}