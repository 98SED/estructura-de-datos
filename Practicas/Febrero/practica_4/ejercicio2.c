/*
** Alumno Programador: Rodrigo Eduardo Delgadillo González
** Fecha: 00-00-2022
**
** Descripción: El programa se encarga de imprimir valores
** almacenados en variables a partir de apuntadores.
*/

#include "practica4.h"

void ejercicio2()
{

    // Declaraciones
    int numero, opt;
    int *ptr_numero = &numero;

    // Mensaje inicial al usuario
    bienvenidap2("  Ejercicio 2  ", "El programa se encarga de imprimir valores  ", "almacenados en variables a partir de apuntadores.        ");
    printf(" ║===========================================================║\n");
    printf(" ║                                                           ║\n");
    printf(" ║ Valores iniciales y dirección de memoria de apuntadores:  ║\n");
    printf(" ║                                                           ║\n");
    printf(" ║===========================================================║\n");
    printf(" ║==================== Variable (numero) ====================║\n");
    printf(" ║===========================================================╝\n");
    printf(" ║ Valor almacenado: %d\n", numero);
    printf(" ║ Dirección de memoria: %p                      ║\n", &numero);
    printf(" ║===========================================================║\n");
    printf(" ║================== Apuntador (ptr_numero) =================║\n");
    printf(" ║===========================================================╝\n");
    printf(" ║ Valor almacenado: %d\n", *ptr_numero);
    printf(" ║ Dirección: %p                                 ║\n", &ptr_numero);
    printf(" ║ Dirección de memoria apuntada: %p             ║\n", ptr_numero);

    do
    {

        menup(&opt);

        switch (opt)
        {
        case 1:
            // Entrada
            entrada();
            printf(" ║ Introduzca un valor para guardarlo en la variable número: \n");
            printf(" ║ *ptr_numero = ");
            scanf("%d", ptr_numero);

            // Proceso y salida
            salida();
            printf(" ║===========================================================║\n");
            printf(" ║                                                           ║\n");
            printf(" ║ Reasignación de variable y direcciones de memoria:        ║\n");
            printf(" ║                                                           ║\n");
            printf(" ║===========================================================║\n");
            printf(" ║==================== Variable (numero) ====================║\n");
            printf(" ║===========================================================╝\n");
            printf(" ║ Valor almacenado: %d\n", numero);
            printf(" ║ Dirección de memoria: %p                      ║\n", &numero);
            printf(" ║===========================================================║\n");
            printf(" ║================== Apuntador (ptr_numero) =================║\n");
            printf(" ║===========================================================╝\n");
            printf(" ║ Valor almacenado: %d\n", *ptr_numero);
            printf(" ║ Dirección: %p                                 ║\n", &ptr_numero);
            printf(" ║ Dirección de memoria apuntada: %p             ║\n", ptr_numero);
            break;
        case 2:
            opt = 0;
        default:
            break;
        }
    } while (opt != 0);
}