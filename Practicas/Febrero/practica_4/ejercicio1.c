/*
** Alumno Programador: Rodrigo Eduardo Delgadillo González
** Fecha: 16-02-2022
**
** Descripción: El presente programa sirve para analizar
** el comportamiento de los apuntadores y los apuntadores
** dobles por medio de la reasignación de una variables a
** partgir de estos.
*/

#include "practica4.h"

void ejercicio1()
{

    // Declaración de variables:
    int valor_e = 95;

    // Declaración de apuntadores:
    int *ptr_valor_e = &valor_e;
    int **dptr_valor_e = &ptr_valor_e;

    bienvenidap3("  Ejercicio 1  ", "En el programa se pone en practica de uso de",
                 "apuntadores simples y dobles, por medio de un breve ejer-",
                 "cicio de reasignación de variables con apuntadores.      ");

    // Proceso y Salida
    salida();
    printf(" ║ Valores iniciales y dirección de memoria de apuntadores:  ║\n");
    printf(" ║                                                           ║\n");
    printf(" ║===========================================================║\n");
    printf(" ║==================== Variable (valor_e) ===================║\n");
    printf(" ║===========================================================║\n");
    printf(" ║ Valor almacenado: %d                                      ║\n", valor_e);
    printf(" ║ Dirección de memoría: %p                      ║\n", &valor_e);
    printf(" ║===========================================================║\n");
    printf(" ║=================  Apuntador (ptr_valor_e) ================║\n");
    printf(" ║===========================================================║\n");
    printf(" ║ Valor almacenado: %d                                      ║\n", *ptr_valor_e);
    printf(" ║ Dirección: %p                                 ║\n", &ptr_valor_e);
    printf(" ║ Dirección de memoría apuntada: %p             ║\n", ptr_valor_e);
    printf(" ║===========================================================║\n");
    printf(" ║============== Doble Apuntador (dptr_valor_e) =============║\n");
    printf(" ║===========================================================║\n");
    printf(" ║ Valor almacenado: %d                                      ║\n", **dptr_valor_e);
    printf(" ║ Dirección: %p                                 ║\n", &dptr_valor_e);
    printf(" ║ Dirección de memoría apuntada: %p             ║\n", dptr_valor_e);
    printf(" ║===========================================================║\n");
    printf(" ║                                                           ║\n");
    printf(" ║ Reasignación de variable por medio de apuntadores:        ║\n");
    printf(" ║                                                           ║\n");
    printf(" ║===========================================================║\n");

    *ptr_valor_e = 105;

    printf(" ║                                                           ║\n");
    printf(" ║ Reasignación de valor en apuntador (*ptr_valor_e = 105;)  ║\n");
    printf(" ║ Valores almacenados (ptr_valor_e, valor_e): %d, %d      ║\n", *ptr_valor_e, valor_e);
    printf(" ║ Direcciones de memoría: (ptr_valor_e, valor_e):           ║\n");
    printf(" ║ ptr_valor_e: %p                               ║\n", ptr_valor_e);
    printf(" ║ valor_e: %p                                   ║\n", &valor_e);
    printf(" ║===========================================================║\n");

    **dptr_valor_e = 99;

    printf(" ║                                                           ║\n");
    printf(" ║ Reasignación de valor en doble apuntador                  ║\n");
    printf(" ║ (**dptr_valor_e = 99)                                     ║\n");
    printf(" ║ Valores almacenados (dptr_valor_e, valor_e): %d, %d       ║\n", **dptr_valor_e, valor_e);
    printf(" ║ Direcciones de memoría:                                   ║\n");
    printf(" ║ dptr_valor_e: %p                              ║\n", dptr_valor_e);
    printf(" ║ valor_e: %p                                   ║\n", &valor_e);
}