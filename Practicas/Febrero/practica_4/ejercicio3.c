/*
** Alumno Programador: Rodrigo Eduardo Delgadillo González
** Fecha: 00-00-2022
**
** Descripción: El presente programa es una copia de la
** diapositiva 14 de la presentación "Unidad III Memoria
** Estática y Dinámica"
*/

#include "practica4.h"

int pasoPorValor(int i);
void pasoPorReferencia(int *ptr_i);

void ejercicio3()
{

    // Declaraciones
    int i = 10;

    // Mensaje inicial al usuario
    bienvenidap2("  Ejercicio 3  ", "El presente programa muestra el uso de      ",
                                    "funciones y apuntadores.                                 ");

    // Proceso y salida
    salida();
    printf(" ║ Paso por valor. El valor de i = %d                        ║\n", i = pasoPorValor(i));
    pasoPorReferencia(&i);
    printf(" ║ Paso por referencia. El valor de i = %d                  ║\n", i);

    return;
}

int pasoPorValor(int i)
{

    return i = 50;
}

void pasoPorReferencia(int *ptr_i)
{

    *ptr_i = 500;
}