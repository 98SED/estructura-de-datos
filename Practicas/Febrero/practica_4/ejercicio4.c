/*
** Alumno Programador: Rodrigo Eduardo Delgadillo González
** Fecha: 16-02-2022
**
** Descripción:
**
**
*/

#include "practica4.h"

void sswap(int *ptr_x, int *ptr_y);

void ejercicio4()
{

    // Declaraciones
    int a = 1, b = 2;

    // Mensaje inicial al usuario
    bienvenidap2("  Ejercicio 4  ", "El programa intercambia los valores de      ",
                                    "dos variables                                            ");

    // Proceso y salida
    salida();
    sswap(&a, &b);
    printf(" ║ a = %d | b = %d                                             ║\n", a, b);


}

void sswap(int *ptr_x, int *ptr_y){

    int tmp;
    tmp = *ptr_x;
    *ptr_x = *ptr_y;
    *ptr_y = tmp;

}