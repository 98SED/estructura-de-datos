/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 03-02-2022
 * Última modificación: 16-02-2022
 *
 * Descripción: Biblioteca practica0.h para la declaración
 * de funciones relativas a los ejercicios de las prácti-
 * cas.
 *
 * Últimos cambios: Nuevos menús de navegación para ejer-
 * cicios complejos (menupi y menups) y delimitadores para
 * la entrada y salida de datos, además de la finalización
 * de ciertos procesos.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

#include "nav.h"

#ifndef _FINAL_PROJECT_H_
#define _FINAL_PROJECT_H_

// Estructura del nodo general:
typedef struct component_stc
{

    // Variables generales:
    char type[50];
    char name[50];
    char brand[50];
    char description[106];
    float price;

    // Apuntadores a los nodos siguiente y anterior:
    component_t *next;
    component_t *prev;

} component_t;

/* typedef struct computer_stc
{

    cpu_t computer_cpu;
    ram_t computer_ram;
    storage_t computer_storage;
    screen_t computer_screen;
    keyboard_t computer_keyboard;
    software_t computer_OS;
    software_t computer_prod_software;
    software_t computer_sec_software;
    warranty_t computer_warranty;
    float price;

} computer_t; */

void insertnode(component_t **origin, char position, char prevtype[50], char type[50], char name[50], char brand[50], char description[106], float price);
float calcprice(component_t *origin);
void printlist(component_t *origin);

#endif