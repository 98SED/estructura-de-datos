/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 00-00-2022
 *
 * Descripción: 
 * 
 * 
 */

/** Borrar hasta marcar todos los pasos:
 * TODO: Ajustar nombres de archivos. []
 * TODO: Ajustar el número de practica en #include "practica0.h" dentro de los archivos: practicaN.c, ejercicioNN_NN.c []
 * TODO: Ajustar las indicaciones de prepocesador en el archivo practicaN.h []
 * TODO: Añadir descripciones a los comentarios iniciales y ajustar las fechas []
 * TODO: Ajustar la fecha y número de practica/ejercicios de las funciones bienv() y bienvprog() []
 * TODO: Declarar las funciones de cada uno de los ejercicios en practicaN.h []
 * TODO: Crear los archivos necesarios para cada uno de los ejercicios. []
 */

#include "final_project.h"

int main()
{

    int opt, quantity;
    component_t *origin = NULL;

    bienv(); /* 10 , 11 */

    insertnode(&origin, 'e', "n/a", "Configuración Inicial", "Latitude 13 3300", "Dell", "Intel Celeron 3865U 1.8 GHz, Windows 10 Home...", 12156.09);
    printlist(origin);
    printf("Total: %f", calcprice(origin));
    insertnode(&origin, 'e', "n/a", "Procesador", "Core i3", "Intel", "2.30GHz", 1851.36);

    /* 
        do
        {
        menpri("", "", "", "", "", &opt); /* 17

        switch (opt)
        {
        case 1:
            standard_config1(&computer, &quantity);
            break;
        case 2:
            standard_config2(&computer, &quantity);
            break;
        case 3:
            standard_config3(&computer, &quantity);
            break;
        case 4:
            // ejercicio4();
            break;
        case 5:
            // ejercicio5();
            break;
        case 6:
            break;
        default:
            msjerr("             La opción ingresada no es valida            ", "               Ingresa una opción valida...              "); /* 57
            break;
        }

        } while (opt != 6); 
    */

    msjfin();

    return 0;

}