/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 00-00-2022
 *
 * Descripción: 
 * 
 * 
 */

#include "final_project.h"

void insertnode(component_t **origin, char position, char prevtype[50], char type[50], char name[50], char brand[50], char description[106], float price)
{

    component_t *new_node, *last_node, *temp = *origin;
    new_node = malloc(1 * sizeof(component_t));

    strcpy(new_node->type, type);
    strcpy(new_node->name, type);
    strcpy(new_node->brand, type);
    strcpy(new_node->description, type);
    new_node->price = price;

    if (*origin == NULL)
    {

        new_node->next = new_node;
        new_node->prev = new_node;

        *origin = new_node;
    }else if (position == 'e' && *origin != NULL)
    {
        new_node->next = *origin;
        new_node->prev = last_node;
        (*origin)->prev = new_node;
        last_node->next = new_node;
    }else if (position == 's' && *origin != NULL)
    {
        new_node->next = *origin;
        new_node->prev = last_node;
        (*origin)->prev = new_node;
        last_node->next = new_node;
        *origin = new_node;
    }else if (position == 'b' && *origin != NULL)
    {
        while (temp->type != prevtype)
        {
            temp = temp->next;
        }

        component_t *next = temp->next;

        temp->next = new_node;
        new_node->prev = temp;
        new_node->next = next;
        next->prev = new_node;
    }
}

float calcprice(component_t *origin)
{
    float price;
    component_t *temp = origin;
    while (temp->next != origin)
    {
        price+=temp->price;
    }

    return price;
}

void printlist(component_t *origin)
{
    component_t *temp = origin;

    do
    {
        printf("%s: %s  %s %s", temp->type, temp->brand, temp->name, temp->description);
        temp = temp->next;
    }while (temp != origin);


}

/* void print_computers(computer_t *computer_ptr, int *quantity)
{
    for (size_t i = 0; i < *quantity; i++)
    {
        printf(" ╠════════════════════════════════════════════════════════════════════════════════════════════════════════════╣\n");
        printf(" ║ ==================================================  CPU  ================================================= ║\n");
        printf(" ╠════════════════════════════════════════════════════════════════════════════════════════════════════════════╝\n");
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ╠════════════════════════════════════════════════════════════════════════════════════════════════════════════╣\n");
        printf(" ║ =================================================   RAM   ================================================ ║\n");
        printf(" ╠════════════════════════════════════════════════════════════════════════════════════════════════════════════╝\n");
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);

        
    }

    printf(" ╠═══╦═════════════════════════════╦═══╦═════════════════════════════════╦═══╦════════════════════════════════╣\n");

    return;

} */

