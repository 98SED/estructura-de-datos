/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 00-00-2022
 *
 * Descripción: 
 * 
 * 
 */

#include "final_project.h"

void standard_config1(computer_t **computer_ptr, int *quantity)
{

    computer_t *computer_arr;

    computer_arr = (computer_t*)calloc(*quantity,sizeof(computer_t));

    if (computer_arr == NULL)
    {
        msjerr("Problemas al reservar memoria para los registros iniciales", "Finalice la ejecución del programa y ejecutelo nuevamente. ");
    }

    // Standard cpu
    strcpy(computer_arr->computer_cpu.brand, "Intel");
    strcpy(computer_arr->computer_cpu.model, "Celeron 3865U");
    strcpy(computer_arr->computer_cpu.gen, "7a");
    strcpy(computer_arr->computer_cpu.cores, "2");
    strcpy(computer_arr->computer_cpu.frequency_GHz, "1.80 [GHz]");
    strcpy(computer_arr->computer_cpu.cache_MB, "2 [MB]");
    computer_arr->computer_cpu.price = 0;

    // Standard RAM
    strcpy(computer_arr->computer_ram.type, "DDR4");
    strcpy(computer_arr->computer_ram.capacity_GB, "4 [GB]");
    strcpy(computer_arr->computer_ram.frequency_MHz, "2666 [MHz]");
    computer_arr->computer_ram.price = 0;

    // Standard storage
    strcpy(computer_arr->computer_storage.type, "eMMC");
    strcpy(computer_arr->computer_storage.capacity_GB, "64 [GB]");
    computer_arr->computer_storage.price = 0;

    // Standard screen
    strcpy(computer_arr->computer_screen.resolution, "DDR4");
    strcpy(computer_arr->computer_screen.aspect_ratio, "4 [GB]");
    strcpy(computer_arr->computer_screen.size_inch, "2666 [MHz]");
    strcpy(computer_arr->computer_screen.specs, "2666 [MHz]");
    computer_arr->computer_screen.price = 0;

    // Standard keyboard
    strcpy(computer_arr->computer_keyboard.language, "ES");
    strcpy(computer_arr->computer_keyboard.distribution, "LATAM");
    strcpy(computer_arr->computer_keyboard.type, "QWERTY");
    strcpy(computer_arr->computer_keyboard.size, "Completo");
    strcpy(computer_arr->computer_keyboard.features, "n/a");
    computer_arr->computer_keyboard.price = 0;

    // Standard OS
    strcpy(computer_arr->computer_OS.brand, "Microsoft");
    strcpy(computer_arr->computer_OS.name, "Windows");
    strcpy(computer_arr->computer_OS.version, "10");
    strcpy(computer_arr->computer_OS.edition, "HOME");
    strcpy(computer_arr->computer_OS.arquitechture_bits, "64 [bit]");
    strcpy(computer_arr->computer_OS.language, "Español");
    computer_arr->computer_OS.price = 0;

    // Standard woftware
    strcpy(computer_arr->computer_prod_software.brand, "Microsoft");
    strcpy(computer_arr->computer_prod_software.name, "Office");
    strcpy(computer_arr->computer_prod_software.version, "n/a");
    strcpy(computer_arr->computer_prod_software.edition, "Evaluación");
    strcpy(computer_arr->computer_prod_software.arquitechture_bits, "64 [bit]");
    strcpy(computer_arr->computer_prod_software.language, "Español");
    computer_arr->computer_prod_software.price = 0;

    strcpy(computer_arr->computer_sec_software.brand, "n/a");
    strcpy(computer_arr->computer_sec_software.name, "n/a");
    strcpy(computer_arr->computer_sec_software.version, "n/a");
    strcpy(computer_arr->computer_sec_software.edition, "n/a");
    strcpy(computer_arr->computer_sec_software.arquitechture_bits, "n/a");
    strcpy(computer_arr->computer_sec_software.language, "n/a");
    computer_arr->computer_sec_software.price = 0;

    // Standard warranty
    strcpy(computer_arr->computer_warranty.time, "1 año");
    strcpy(computer_arr->computer_warranty.country, "LATAM");
    strcpy(computer_arr->computer_warranty.details, "Servicio doméstico después de revisión remota.");
    computer_arr->computer_warranty.price = 0;

    //standard price
    computer_arr->price = 12156.09;

    *computer_ptr = computer_arr;

    return;
}

void standard_config2(computer_t **computer_ptr, int *quantity)
{

    computer_t *computer_arr;

    computer_arr = (computer_t*)calloc(*quantity,sizeof(computer_t));

    if (computer_arr == NULL)
    {
        msjerr("Problemas al reservar memoria para los registros iniciales", "Finalice la ejecución del programa y ejecutelo nuevamente. ");
    }

    // Standard cpu
    strcpy(computer_arr->computer_cpu.brand, "Intel");
    strcpy(computer_arr->computer_cpu.model, "Celeron 3865U");
    strcpy(computer_arr->computer_cpu.gen, "7a");
    strcpy(computer_arr->computer_cpu.cores, "2");
    strcpy(computer_arr->computer_cpu.frequency_GHz, "1.80 [GHz]");
    strcpy(computer_arr->computer_cpu.cache_MB, "2 [MB]");
    computer_arr->computer_cpu.price = 0;

    // Standard RAM
    strcpy(computer_arr->computer_ram.type, "DDR4");
    strcpy(computer_arr->computer_ram.capacity_GB, "4 [GB]");
    strcpy(computer_arr->computer_ram.frequency_MHz, "2666 [MHz]");
    computer_arr->computer_ram.price = 0;

    // Standard storage
    strcpy(computer_arr->computer_storage.type, "eMMC");
    strcpy(computer_arr->computer_storage.capacity_GB, "64 [GB]");
    computer_arr->computer_storage.price = 0;

    // Standard screen
    strcpy(computer_arr->computer_screen.resolution, "DDR4");
    strcpy(computer_arr->computer_screen.aspect_ratio, "4 [GB]");
    strcpy(computer_arr->computer_screen.size_inch, "2666 [MHz]");
    strcpy(computer_arr->computer_screen.specs, "2666 [MHz]");
    computer_arr->computer_screen.price = 0;

    // Standard keyboard
    strcpy(computer_arr->computer_keyboard.language, "ES");
    strcpy(computer_arr->computer_keyboard.distribution, "LATAM");
    strcpy(computer_arr->computer_keyboard.type, "QWERTY");
    strcpy(computer_arr->computer_keyboard.size, "Completo");
    strcpy(computer_arr->computer_keyboard.features, "n/a");
    computer_arr->computer_keyboard.price = 0;

    // Standard OS
    strcpy(computer_arr->computer_OS.brand, "Microsoft");
    strcpy(computer_arr->computer_OS.name, "Windows");
    strcpy(computer_arr->computer_OS.version, "10");
    strcpy(computer_arr->computer_OS.edition, "HOME");
    strcpy(computer_arr->computer_OS.arquitechture_bits, "64 [bit]");
    strcpy(computer_arr->computer_OS.language, "Español");
    computer_arr->computer_OS.price = 0;

    // Standard woftware
    strcpy(computer_arr->computer_prod_software.brand, "Microsoft");
    strcpy(computer_arr->computer_prod_software.name, "Office");
    strcpy(computer_arr->computer_prod_software.version, "n/a");
    strcpy(computer_arr->computer_prod_software.edition, "Evaluación");
    strcpy(computer_arr->computer_prod_software.arquitechture_bits, "64 [bit]");
    strcpy(computer_arr->computer_prod_software.language, "Español");
    computer_arr->computer_prod_software.price = 0;

    strcpy(computer_arr->computer_sec_software.brand, "n/a");
    strcpy(computer_arr->computer_sec_software.name, "n/a");
    strcpy(computer_arr->computer_sec_software.version, "n/a");
    strcpy(computer_arr->computer_sec_software.edition, "n/a");
    strcpy(computer_arr->computer_sec_software.arquitechture_bits, "n/a");
    strcpy(computer_arr->computer_sec_software.language, "n/a");
    computer_arr->computer_sec_software.price = 0;

    // Standard warranty
    strcpy(computer_arr->computer_warranty.time, "1 año");
    strcpy(computer_arr->computer_warranty.country, "LATAM");
    strcpy(computer_arr->computer_warranty.details, "Servicio doméstico después de revisión remota.");
    computer_arr->computer_warranty.price = 0;

    //standard price
    computer_arr->price = 12156.09;

    *computer_ptr = computer_arr;

    return;
}

void standard_config3(computer_t **computer_ptr, int *quantity)
{

    computer_t *computer_arr;

    computer_arr = (computer_t*)calloc(*quantity,sizeof(computer_t));

    if (computer_arr == NULL)
    {
        msjerr("Problemas al reservar memoria para los registros iniciales", "Finalice la ejecución del programa y ejecutelo nuevamente. ");
    }

    // Standard cpu
    strcpy(computer_arr->computer_cpu.brand, "Intel");
    strcpy(computer_arr->computer_cpu.model, "Celeron 3865U");
    strcpy(computer_arr->computer_cpu.gen, "7a");
    strcpy(computer_arr->computer_cpu.cores, "2");
    strcpy(computer_arr->computer_cpu.frequency_GHz, "1.80 [GHz]");
    strcpy(computer_arr->computer_cpu.cache_MB, "2 [MB]");
    computer_arr->computer_cpu.price = 0;

    // Standard RAM
    strcpy(computer_arr->computer_ram.type, "DDR4");
    strcpy(computer_arr->computer_ram.capacity_GB, "4 [GB]");
    strcpy(computer_arr->computer_ram.frequency_MHz, "2666 [MHz]");
    computer_arr->computer_ram.price = 0;

    // Standard storage
    strcpy(computer_arr->computer_storage.type, "eMMC");
    strcpy(computer_arr->computer_storage.capacity_GB, "64 [GB]");
    computer_arr->computer_storage.price = 0;

    // Standard screen
    strcpy(computer_arr->computer_screen.resolution, "DDR4");
    strcpy(computer_arr->computer_screen.aspect_ratio, "4 [GB]");
    strcpy(computer_arr->computer_screen.size_inch, "2666 [MHz]");
    strcpy(computer_arr->computer_screen.specs, "2666 [MHz]");
    computer_arr->computer_screen.price = 0;

    // Standard keyboard
    strcpy(computer_arr->computer_keyboard.language, "ES");
    strcpy(computer_arr->computer_keyboard.distribution, "LATAM");
    strcpy(computer_arr->computer_keyboard.type, "QWERTY");
    strcpy(computer_arr->computer_keyboard.size, "Completo");
    strcpy(computer_arr->computer_keyboard.features, "n/a");
    computer_arr->computer_keyboard.price = 0;

    // Standard OS
    strcpy(computer_arr->computer_OS.brand, "Microsoft");
    strcpy(computer_arr->computer_OS.name, "Windows");
    strcpy(computer_arr->computer_OS.version, "10");
    strcpy(computer_arr->computer_OS.edition, "HOME");
    strcpy(computer_arr->computer_OS.arquitechture_bits, "64 [bit]");
    strcpy(computer_arr->computer_OS.language, "Español");
    computer_arr->computer_OS.price = 0;

    // Standard woftware
    strcpy(computer_arr->computer_prod_software.brand, "Microsoft");
    strcpy(computer_arr->computer_prod_software.name, "Office");
    strcpy(computer_arr->computer_prod_software.version, "n/a");
    strcpy(computer_arr->computer_prod_software.edition, "Evaluación");
    strcpy(computer_arr->computer_prod_software.arquitechture_bits, "64 [bit]");
    strcpy(computer_arr->computer_prod_software.language, "Español");
    computer_arr->computer_prod_software.price = 0;

    strcpy(computer_arr->computer_sec_software.brand, "n/a");
    strcpy(computer_arr->computer_sec_software.name, "n/a");
    strcpy(computer_arr->computer_sec_software.version, "n/a");
    strcpy(computer_arr->computer_sec_software.edition, "n/a");
    strcpy(computer_arr->computer_sec_software.arquitechture_bits, "n/a");
    strcpy(computer_arr->computer_sec_software.language, "n/a");
    computer_arr->computer_sec_software.price = 0;

    // Standard warranty
    strcpy(computer_arr->computer_warranty.time, "1 año");
    strcpy(computer_arr->computer_warranty.country, "LATAM");
    strcpy(computer_arr->computer_warranty.details, "Servicio doméstico después de revisión remota.");
    computer_arr->computer_warranty.price = 0;

    //standard price
    computer_arr->price = 12156.09;

    *computer_ptr = computer_arr;

    return;
}

/* void print_computers(computer_t *computer_ptr, int *quantity)
{
    for (size_t i = 0; i < *quantity; i++)
    {
        printf(" ╠════════════════════════════════════════════════════════════════════════════════════════════════════════════╣\n");
        printf(" ║ ==================================================  CPU  ================================================= ║\n");
        printf(" ╠════════════════════════════════════════════════════════════════════════════════════════════════════════════╝\n");
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ╠════════════════════════════════════════════════════════════════════════════════════════════════════════════╣\n");
        printf(" ║ =================================================   RAM   ================================================ ║\n");
        printf(" ╠════════════════════════════════════════════════════════════════════════════════════════════════════════════╝\n");
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);
        printf(" ║ : %s\n", computer_ptr->);

        
    }

    printf(" ╠═══╦═════════════════════════════╦═══╦═════════════════════════════════╦═══╦════════════════════════════════╣\n");

    return;

} */

