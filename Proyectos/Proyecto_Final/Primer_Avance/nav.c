/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 03-02-2022
 * Última modificación: 16-02-2022
 *
 * Descripción: Archivo auxiliar de la biblioteca general.h
 * con definiciones de funciones relativas a menus de nave-
 * gación CUI para emuladores de terminal.
 */

#include "nav.h"

// ! Biblio string strcpy memoria dinamica

// Mensajes de bienvenida:

void welcome()
{
    printf("\n");
    printf(" ╔════════════════════════════════════════════════════════════════════════════════════════════════════════════╗\n");
    printf(" ║                                                                                                            ║\n");
    printf(" ║                 ██████╗░██╗███████╗███╗░░██╗██╗░░░██╗███████╗███╗░░██╗██╗██████╗░░█████╗░                  ║\n");
    printf(" ║                 ██╔══██╗██║██╔════╝████╗░██║██║░░░██║██╔════╝████╗░██║██║██╔══██╗██╔══██╗                  ║\n");
    printf(" ║                 ██████╦╝██║█████╗░░██╔██╗██║╚██╗░██╔╝█████╗░░██╔██╗██║██║██║░░██║██║░░██║                  ║\n");
    printf(" ║                 ██╔══██╗██║██╔══╝░░██║╚████║░╚████╔╝░██╔══╝░░██║╚████║██║██║░░██║██║░░██║                  ║\n");
    printf(" ║                 ██████╦╝██║███████╗██║░╚███║░░╚██╔╝░░███████╗██║░╚███║██║██████╔╝╚█████╔╝                  ║\n");
    printf(" ║                 ╚═════╝░╚═╝╚══════╝╚═╝░░╚══╝░░░╚═╝░░░╚══════╝╚═╝░░╚══╝╚═╝╚═════╝░░╚════╝░                  ║\n");
    printf(" ║                                                                                                            ║\n");
    printf(" ║                                                  ░█████╗░                                                  ║\n");
    printf(" ║                                                  ██╔══██╗                                                  ║\n");
    printf(" ║                                                  ███████║                                                  ║\n");
    printf(" ║                                                  ██╔══██║                                                  ║\n");
    printf(" ║                                                  ██║░░██║                                                  ║\n");
    printf(" ║                                                  ╚═╝░░╚═╝                                                  ║\n");
    printf(" ║                                                                                                            ║\n");
    printf(" ║                                                                                                            ║\n");
    printf(" ║                   ████████╗███████╗░█████╗░██╗░░██╗███╗░░░███╗░█████╗░██████╗░██╗░░██╗████████╗            ║\n");
    printf(" ║                   ╚══██╔══╝██╔════╝██╔══██╗██║░░██║████╗░████║██╔══██╗██╔══██╗██║░██╔╝╚══██╔══╝            ║\n");
    printf(" ║                   ░░░██║░░░█████╗░░██║░░╚═╝███████║██╔████╔██║███████║██████╔╝█████═╝░░░░██║░░░            ║\n");
    printf(" ║                   ░░░██║░░░██╔══╝░░██║░░██╗██╔══██║██║╚██╔╝██║██╔══██║██╔══██╗██╔═██╗░░░░██║░░░            ║\n");
    printf(" ║                   ░░░██║░░░███████╗╚█████╔╝██║░░██║██║░╚═╝░██║██║░░██║██║░░██║██║░╚██╗░░░██║░░░            ║\n");
    printf(" ║                   ░░░╚═╝░░░╚══════╝░╚════╝░╚═╝░░╚═╝╚═╝░░░░░╚═╝╚═╝░░╚═╝╚═╝░░╚═╝╚═╝░░╚═╝░░░╚═╝░░░            ║\n");
    printf(" ║                                                                                                            ║\n");
    printf(" ╠════════════════════════════════════════════════════════════════════════════════════════════════════════════╣\n");
    printf(" ║                                                                                                            ║\n");
    printf(" ║ Selecciona uno de los siguientes equipos preconfigurados:                                                  ║\n");
    printf(" ║   NOTA: Más adelante prodrás ajustar parametros como la cantidad de dispositivos o sus componentes.        ║\n");
    printf(" ║                                                                                                            ║\n");
    printf(" ╠════════════════════════════════════════════════════════════════════════════════════════════════════════════╣\n");
}

/* void bienvprog1(char str_programa[16], char str_descln1[45])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", str_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", str_descln1);

}

void bienvprog2(char str_programa[16], char str_descln1[45], char str_descln2[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", str_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", str_descln1);
    printf(" ║ %s ║\n", str_descln2);

}

void bienvprog3(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", str_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", str_descln1);
    printf(" ║ %s ║\n", str_descln2);
    printf(" ║ %s ║\n", str_descln3);

}

void bienvprog4(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58], char str_descln4[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", str_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", str_descln1);
    printf(" ║ %s ║\n", str_descln2);
    printf(" ║ %s ║\n", str_descln3);
    printf(" ║ %s ║\n", str_descln4);

}

void bienvprog5(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58], char str_descln4[58], char str_descln5[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", str_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", str_descln1);
    printf(" ║ %s ║\n", str_descln2);
    printf(" ║ %s ║\n", str_descln3);
    printf(" ║ %s ║\n", str_descln4);
    printf(" ║                                                           ║\n");
    printf(" ║ %s ║\n", str_descln5);

}

void bienvprog6(char str_programa[16], char str_descln1[45], char str_descln2[58], char str_descln3[58], char str_descln4[58], char str_descln5[58], char str_descln6[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ==================== %s ==================== ║\n", str_programa);
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ Descripción: %s ║\n", str_descln1);
    printf(" ║ %s ║\n", str_descln2);
    printf(" ║ %s ║\n", str_descln3);
    printf(" ║ %s ║\n", str_descln4);
    printf(" ║                                                           ║\n");
    printf(" ║ %s ║\n", str_descln5);
    printf(" ║ %s ║\n", str_descln6);

}

// Mensajes de proceso

void msjent()
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ======================== ENTRADA ======================== ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╝\n");
}

void msjsal()
{

    printf(" ╠═══════════════════════════════════════════════════════════╗\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ======================== SALIDA ========================= ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");

}

void msjfin()
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ║ ========================== FIN ========================== ║\n");
    printf(" ║ ========================================================= ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ La ejecución del programa finalizó, gracias por probarlo. ║\n");
    printf(" ║ Dudas, comentarios y sugerencias: red_g4898@outlook.com   ║\n");
    printf(" ╚═══════════════════════════════════════════════════════════╝\n");
}

void msjfinprog()
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ ========================== FIN ========================== ║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ El programa anterior terminó de ejecutarse, selecciona    ║\n");
    printf(" ║ otra opción disponible:                                   ║\n");
    printf(" ╠═══════════════════╦═══════════════════╦═══════════════════╣\n");
}

void msjerr(char causa[58], char solucion[58])
{

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║===========================================================║\n");
    printf(" ║==========¡ERROR!=========¡ERROR!=========¡ERROR!==========║\n");
    printf(" ║===========================================================║\n");
    printf(" ╠═══════════════════════════════════════════════════════════╣\n");
    printf(" ║ %s ║\n", causa);
    printf(" ║ %s ║\n", solucion);
    printf(" ╠═══════════════════╦═══════════════════╦═══════════════════╣\n");
}

void msjpros1(char msj[58]){

    printf(" ║ ========================================================= ║\n");
    printf(" ║                                                           ║\n");
    printf(" ║ %s ║\n", msj);
    printf(" ║                                                           ║\n");
    printf(" ║ ========================================================= ║\n");

}

void msjpros2(char msj1[58], char msj2[58]){

    printf(" ║ ========================================================= ║\n");
    printf(" ║                                                           ║\n");
    printf(" ║ %s ║\n", msj1);
    printf(" ║ %s ║\n", msj2);
    printf(" ║                                                           ║\n");
    printf(" ║ ========================================================= ║\n");

}

// Menus de navegación

void menpri(char str_cas1[18], char str_cas2[18], char str_cas3[18], char str_cas4[18], char str_cas5[18], int *ptr_opt)
{

    printf(" ║ %s ║ %s ║ %s ║\n", str_cas1, str_cas2, str_cas3);
    printf(" ╠═══════════════════╬═══════════════════╬═══════════════════╣\n");
    printf(" ║ %s ║ %s ║     6 - Salir     ║\n", str_cas4, str_cas5);
    printf(" ╠═══════════════════╩═══════════════════╩═══════════════════╝\n");
    printf(" ║ Ingresa una opción: ");
    scanf("%d", ptr_opt);
}

void menprog(char str_cas1[18], char str_cas2[18], char str_cas3[18], int *ptr_opt)
{

    printf(" ╠═══════════════════╦═══════════════════╦═══════════════════╣\n");
    printf(" ║ %s ║ %s ║ %s ║\n", str_cas1, str_cas2, str_cas3);
    printf(" ╠═══════════════════╩═══════════════════╩═══════════════════╝\n");
    printf(" ║ Ingresa una opción: ");
    scanf("%d", ptr_opt);
}

// Lineas y saltos de linea.

void lnb(){

    printf(" ║                                                           ║\n");

}

void lns(){

    printf(" ╠═══════════════════════════════════════════════════════════╣\n");

}

void lnstr(char str[57]){
    printf(" ║ % ║ \n", str);
}

void lnstra(char str[57]){
    printf(" ║ % \n", str);
}

void slt(){

    printf(" ║ \n");

}

// Entradas de datos:

void entint(char str[57], int *ptr_int){

    printf(" ║ %s: ", str);
    scanf("%d", ptr_int);

}

void entfloat(char str[57], float *ptr_float){

    printf(" ║ %s: ", str);
    scanf("%f", ptr_float);

}

void entdouble(char str[57], double *ptr_double){

    printf(" ║ %s: ", str);
    scanf("%f", ptr_double);

}

void entstring(char str[57], char *entstr, int longitud){

    entstr = (char*)calloc(longitud, sizeof(char));

    printf(" ║ %s: ", str);
    fgets(entstr, longitud, stdin);

} */
