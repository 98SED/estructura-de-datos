/**
 * Alumno Programador: Rodrigo Eduardo Delgadillo González
 * Fecha: 03-02-2022
 * Última modificación: 16-02-2022
 *
 * Descripción: Biblioteca practica0.h para la declaración
 * de funciones relativas a los ejercicios de las prácti-
 * cas.
 *
 * Últimos cambios: Nuevos menús de navegación para ejer-
 * cicios complejos (menupi y menups) y delimitadores para
 * la entrada y salida de datos, además de la finalización
 * de ciertos procesos.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

#include "nav.h"

#ifndef _FINAL_PROJECT_H_
#define _FINAL_PROJECT_H_

typedef struct cpu_stc
{
    char brand[10];
    char model[10];
    char gen[5];
    char cores[3];
    char frequency_GHz[12];
    char cache_MB[8];
    float price;

} cpu_t;

typedef struct ram_stc
{
    char type[6];
    char capacity_GB[9];
    char frequency_MHz[12];
    float price;

} ram_t;

typedef struct storage_stc
{
    char type[10];
    char capacity_GB[11];
    float price;

} storage_t;

typedef struct screen_stc
{
    char resolution[7];
    char aspect_ratio[10];
    char size_inch[3];
    char specs[60];
    float price;

} screen_t;

typedef struct keyboard_stc
{
    char language[3];
    char distribution[11];
    char type[7];
    char size[11];
    char features[25];
    float price;

} keyboard_t;

typedef struct software_stc
{
    char brand[12];
    char name[10];
    char version[10];
    char edition[13];
    char arquitechture_bits[10];
    char language[10];
    float price;

} software_t;

typedef struct warranty_stc
{
    char time[7];
    char country[15];
    char details[100];
    float price;

} warranty_t;

typedef struct computer_stc
{

    cpu_t computer_cpu;
    ram_t computer_ram;
    storage_t computer_storage;
    screen_t computer_screen;
    keyboard_t computer_keyboard;
    software_t computer_OS;
    software_t computer_prod_software;
    software_t computer_sec_software;
    warranty_t computer_warranty;
    float price;

} computer_t;

void standard_config1(computer_t **computer_ptr, int *quantity);
void standard_config2(computer_t **computer_ptr, int *quantity);
void standard_config3(computer_t **computer_ptr, int *quantity);

#endif